<?php
/**
 * @author Fabian Fülling <fabian@fabfuel.de>
 * @created 02.02.14, 09:19 
 */
namespace Mongo;

abstract class LazyAbstract
{
    /**
     * @var array
     */
    protected $data = [];

    /**
     * @var bool
     */
    protected $initialized = false;

    /**
     * @return mixed $data
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @return boolean $data
     */
    public function hasData()
    {
        return !empty($this->data);
    }

    /**
     * @param array $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }

    /**
     * @return boolean
     */
    public function isInitialized()
    {
        return $this->initialized;
    }

    /**
     * @param bool $initialized
     * @return bool
     */
    public function setInitialized($initialized = true)
    {
        return $this->initialized = $initialized;
    }
}
