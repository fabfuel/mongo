<?php
/**
 * @author Fabian Fülling <fabian@fabfuel.de>
 * @created 30.01.14
 */

namespace Mongo;

use Mongo\Cache\CacheInterface;
use Mongo\Cache\Memory;
use Mongo\Profiler\ProfilerInterface;

class Db
{
    const IDENTIFIER_DEFAULT = 'default';

    /**
     * @var \MongoDB
     */
    protected $database;

    /**
     * @var \MongoClient
     */
    protected $client;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var ProfilerInterface
     */
    protected $profiler;

    /**
     * @var array
     */
    protected static $instances = [];

    /**
     * @var CacheInterface
     */
    protected static $cache;

    /**
     * @var []
     */
    protected static $collections;

    /**
     * @param \MongoClient $client
     * @param string $name
     * @param string $identifier
     */
    public function __construct(\MongoClient $client, $name, $identifier = self::IDENTIFIER_DEFAULT)
    {
        $this->setClient($client);
        $this->setName($name);
        static::$instances[$identifier] = $this;
        return $this;
    }

    /**
     * @param string $method
     * @param array $arguments
     * @return mixed
     */
    public function __call($method, $arguments)
    {
        return call_user_func_array(
            array($this->getDb(), $method),
            $arguments
        );
    }

    /**
     * @param string $identifier
     * @return static
     * @throws \InvalidArgumentException
     */
    public static function getInstance($identifier = self::IDENTIFIER_DEFAULT)
    {
        if (!isset(static::$instances[$identifier])) {
            throw new \InvalidArgumentException('Unknown database: "' . $identifier . '"');
        }
        return static::$instances[$identifier];
    }

    /**
     * @param string $className
     * @return Collection
     * @throws \InvalidArgumentException
     */
    public function getCollection($className)
    {
        if (!isset(static::$collections[$className])) {
            $collection = new $className($this);
            if (!$collection instanceof Collection) {
                throw new \InvalidArgumentException($className . ' is not a \Mongo\Collection child class');
            }
            static::$collections[$className] = $collection;
        }
        return static::$collections[$className];
    }

    /**
     * @param \MongoClient $client
     */
    public function setClient($client)
    {
        $this->client = $client;
    }

    /**
     * @return \MongoClient
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param CacheInterface $cache
     */
    public static function setCache(CacheInterface $cache)
    {
        static::$cache = $cache;
    }

    /**
     * @return \Mongo\Cache\CacheInterface
     */
    public static function getCache()
    {
        if (is_null(static::$cache)) {
            static::$cache = new Memory();
        }
        return static::$cache;
    }

    /**
     * @param ProfilerInterface $profiler
     */
    public function setProfiler(ProfilerInterface $profiler)
    {
        $this->profiler = $profiler;
    }

    /**
     * @return ProfilerInterface
     */
    public function getProfiler()
    {
        return $this->profiler;
    }

    /**
     * @return \MongoDB
     */
    public function getDb()
    {
        if (is_null($this->database)) {
            $this->database = new \MongoDB(
                $this->getClient(),
                $this->getName()
            );
        }
        return $this->database;
    }

    /**
     * @param \MongoDB $database
     */
    public function setDb($database)
    {
        $this->database = $database;
    }

    /**
     * @param string $name Unique identifier like e.g. Class::Method (\Foobar\MyClass::doSomething)
     * @param array $metadata Addtional interesting metadata for this benchmark
     * @return string|null Identifier token
     */
    public function startProfiler($name, array $metadata = [])
    {
        if (!$this->getProfiler()) {
            return;
        }
        return $this->getProfiler()->start($name, $metadata);
    }

    /**
     * @param string $token
     */
    public function stopProfiler($token)
    {
        if ($token && $this->getProfiler()) {
            $this->getProfiler()->stop($token);
        }
    }
}
