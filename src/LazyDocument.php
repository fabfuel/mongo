<?php
/**
 * @author Fabian Fülling <fabian@fabfuel.de>
 * @created 01.02.14, 18:15 
 */
namespace Mongo;

use OC\Util\ArrayHelper;

abstract class LazyDocument extends LazyAbstract implements LazyInitializable
{
    /**
     * @var array
     */
    protected $additionalData = [];

    /**
     * @var array
     */
    protected $references = [];

    /**
     * @var \MongoId
     */
    protected $mongoId;

    /**
     * @var Db
     */
    protected $database;

    /**
     * @param string $method
     * @param array $arguments
     * @return mixed
     * @throws \BadMethodCallException
     */
    public function __call($method, array $arguments)
    {
        $this->initialize();

        preg_match('/^([a-z]+)(.+)/', $method, $matches);
        $action = $matches[1];
        $field = lcfirst($matches[2]);

        if (!$this->hasField($field)) {
            throw new \BadMethodCallException(
                sprintf('Unknown field: "%s" - called: %s(%s)', $field, $method, implode(', ', $arguments))
            );
        }
        if ($this->{$field} instanceof LazyInitializable) {
            $this->{$field}->initialize();
        }
        switch ($action) {
            case 'get':
                return $this->get($field);
            case 'set':
                $this->set($field, isset($arguments[0]) ? $arguments[0] : null);
                return $this;
            case 'has':
            case 'is':
                return !empty($this->{$field});
            default:
                throw new \BadMethodCallException(sprintf('Unknown method: %s (unknown field: %s)', $method, $field));
        }
    }

    /**
     * @param string $field
     * @param bool $getReference
     * @return mixed
     */
    protected function get($field, $getReference = true)
    {
        if (!$field instanceof Field) {
            $field = $this->findField($field);
        }
        if ($getReference === true && $field->isReference()) {
            return $this->getReferencedObject($field);
        }
        return $this->{$field->getName()};
    }

    /**
     * @param Field $field
     * @return Document
     */
    protected function getReferencedObject(Field $field)
    {
        $fieldName = $field->getName();
        if (!isset($this->{$fieldName})) {
            return null;
        }
        if (!isset($this->references[$fieldName])) {
            $collection = $this->getCollection($field);
            $reference = $collection->findById($this->{$fieldName});
            $this->references[$fieldName] = $reference;
        }
        return $this->references[$fieldName];
    }

    /**
     * @param Field $field
     * @return Collection
     */
    protected function getCollection(Field $field)
    {
        $collectionClass = $field->getReferenceClass();
        return $this->getDatabase()->getCollection($collectionClass);
    }

    /**
     * @param Field|string $fieldname
     * @param mixed $value
     * @throws \RuntimeException
     * @throws \InvalidArgumentException
     */
    protected function set($fieldname, $value)
    {
        if ($fieldname instanceof Field) {
            $field = $fieldname;
        } else {
            $field = $this->findField($fieldname);
        }

        if ($field && $field->isReference()) {
            $documentClass = $this->getCollection($field)->getDocumentClassName();
            if (is_object($value) && '\\'.get_class($value) === $documentClass) {
                if (!$value->getId()) {
                    throw new \RuntimeException('Reference does not have an id');
                }
                $this->references[$field->getName()] = $value;
                $value = $value->getId();
            } elseif ($value instanceof \MongoId) {
                $this->references[$field->getName()] = null;
            } elseif (!is_null($value)) {
                throw new \InvalidArgumentException(sprintf(
                    'Invalid document! Has to be \MongoId or %s, but %s given',
                    $documentClass,
                    is_object($value) ? '\\' . get_class($value) : getType($value)
                ));
            }
        } elseif ($field && $field->isCollection() && !$value instanceof LazyInitializable) {
            $value = $field->getCollectionInstance($value);
        } elseif ($field && $field->isClass() && !$value instanceof LazyInitializable) {
            $value = $field->getInstance($value);
        }
        if ($field instanceof Field) {
            $fieldname = $field->getName();
        } else {
            $fieldname = $field;
        }

        $this->{$fieldname} = $value;
    }

    /**
     * @return boolean
     * @throws \LogicException
     */
    public function initialize()
    {
        if ($this->isInitialized()) {
            return true;
        }
        foreach ($this->getFields() as $fieldname => $field) {
            $value = isset($this->data[$fieldname]) ? $this->data[$fieldname] : null;
            foreach ($field->getAliases() as $alias) {
                if (isset($this->data[$alias])) {
                    $value = $this->data[$alias];
                }
            }
            if (is_null($value)) {
                $value = $field->getDefaultValue();
            }
            $this->set($field, $value);
        }

        $this->additionalData = array_diff_key($this->getData(), array_flip($this->getFieldNames(true)));

        $this->setInitialized();
        return true;
    }

    /**
     * Set Document recursive from a recursive array or a flat array with dot-notation
     *
     * @param array $array
     * @param bool $mergeRecursive
     */
    public function fromArray(array $array, $mergeRecursive = true)
    {
        $array = ArrayHelper::fromDotNotation($array);

        $this->initialize();

        foreach ($this->getFields(true) as $fieldName => $field) {
            if (isset($array[$fieldName])) {
                if ($mergeRecursive && $this->{$field->getName()} instanceof Subdocument) {
                    $this->{$field->getName()}->fromArray($array[$fieldName], $mergeRecursive);
                } else {
                    $this->set($field, $array[$fieldName]);
                }
            }
        }
    }

    /**
     * @return array
     */
    public function toArray()
    {
        if (!$this->isInitialized()) {
            return $this->getData();
        }

        $array = [];

        if ($this->getId()) {
            $array['_id'] = $this->getId();
        }

        foreach ($this->getFields() as $field) {
            if ($this->{$field->getName()} instanceof LazyInitializable) {
                $subarray = $this->{$field->getName()}->toArray();
                if ($subarray) {
                    $array[$field->getName()] = $subarray;
                }
            } elseif ($this->get($field->getName(), false) !== null && $this->get($field->getName(), false) !== '') {
                $array[$field->getName()] = $this->get($field->getName(), false);
            }
        }
        $array = array_merge($array, $this->additionalData);
        return $array;
    }

    /**
     * @param bool $includeAliases
     * @return Field[]
     */
    public function getFields($includeAliases = false)
    {
        $className = get_called_class();
        $cache = Db::getCache();
        $cacheKeyFields = 'fields\\' . $className;
        $cacheKeyAliases = 'aliases\\' . $className;

        if (!$cache->exists($cacheKeyFields)) {

            $fields = [];
            $aliases = [];

            $class = new \ReflectionClass($className);
            $defaultValues = $class->getDefaultProperties();

            $properties = $class->getProperties(\ReflectionProperty::IS_PROTECTED);

            foreach ($properties as $property) {
                if ($property->getDeclaringClass()->getName() != $className) {
                    continue;
                }
                $field = new Field($property);

                if (isset($defaultValues[$property->getName()])) {
                    $field->setDefaultValue($defaultValues[$property->getName()]);
                }

                $fields[$property->getName()] = $field;
                foreach ($field->getAliases() as $alias) {
                    $aliases[$alias] = $field;
                }
            }

            $cache->set($cacheKeyFields, $fields);
            $cache->set($cacheKeyAliases, $aliases);
        }

        if ($includeAliases === true) {
            return array_merge(
                $cache->get($cacheKeyFields),
                $cache->get($cacheKeyAliases)
            );
        }
        return $cache->get($cacheKeyFields);
    }

    /**
     * Get all field names including aliases
     *
     * @return array
     */
    protected function getFieldNames()
    {
        $className = get_called_class();
        $cache = Db::getCache();
        $cacheKeyFields = 'fields\\' . $className;
        $cacheKeyAliases = 'aliases\\' . $className;
        $cacheKeyFieldnames = 'fieldnames\\' . $className;

        if (!$cache->exists($cacheKeyFieldnames)) {
            $cache->set($cacheKeyFieldnames, array_merge(
                ['_id'],
                array_keys($cache->get($cacheKeyFields)),
                array_keys($cache->get($cacheKeyAliases))
            ));
        }
        return $cache->get($cacheKeyFieldnames);
    }

    /**
     * Find a field by its name
     *
     * @param string $fieldName
     * @return Field
     */
    public function findField($fieldName)
    {
        $fields = $this->getFields();
        return isset($fields[$fieldName]) ? $fields[$fieldName] : null;
    }

    /**
     * @param string $field
     * @return bool
     */
    protected function hasField($field)
    {
        return array_key_exists($field, $this->getFields());
    }

    /**
     * @param mixed $mongoId
     */
    public function setId($mongoId)
    {
        $this->mongoId = $mongoId;
    }

    /**
     * @return \MongoId
     */
    public function getId()
    {
        return $this->mongoId;
    }

    /**
     * @param array $data
     */
    public function setData($data)
    {
        if (is_array($data)) {
            parent::setData($data);
            if (isset($data['_id'])) {
                $this->setId($data['_id']);
            }
        }
    }

    /**
     * @return array
     */
    public function getSchema()
    {
        $schema = [];
        foreach ($this->getFields() as $field) {
            $schema[$field->getName()] = $field->getSchema();
        }
        return $schema;
    }

    /**
     * @return Db
     */
    public function getDatabase()
    {
        if (is_null($this->database)) {
            $this->database = Db::getInstance();
        }
        return $this->database;
    }

    /**
     * @param Db $database
     */
    public function setDatabase($database)
    {
        $this->database = $database;
    }
}
