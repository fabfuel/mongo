<?php
/**
 * @author @fabfuel <fabian@fabfuel.de>
 * @created 03.02.14, 07:33 
 */

namespace Mongo;

use Mongo\Field\CollectionField;
use Mongo\Field\Date;
use Mongo\Field\GeoJson\GeoAbstract;

class Field
{
    const TYPE_ARRAY = 'array';
    const TYPE_BOOLEAN = 'boolean';
    const TYPE_FLOAT = 'double';
    const TYPE_INTEGER = 'integer';
    const TYPE_STRING = 'string';

    /**
     * @var \ReflectionProperty
     */
    protected $property;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var Document
     */
    protected $reference;

    /**
     * @var
     */
    protected $referenceClass;

    /**
     * @var string
     */
    protected $classname;

    /**
     * @var array
     */
    protected $aliases = [];

    /**
     * @var mixed
     */
    protected $defaultValue;

    /**
     * @var string
     */
    protected $type;

    /**
     * @var array
     */
    protected $validTypes = [
        'bool'      => self::TYPE_BOOLEAN,
        'boolean'   => self::TYPE_BOOLEAN,
        'int'       => self::TYPE_INTEGER,
        'integer'   => self::TYPE_INTEGER,
        'float'     => self::TYPE_FLOAT,
        'double'    => self::TYPE_FLOAT,
        'string'    => self::TYPE_STRING,
        'array'     => self::TYPE_ARRAY,
    ];

    /**
     * @var array
     */
    protected $annotations = [];

    /**
     * @param \ReflectionProperty $property
     */
    public function __construct(\ReflectionProperty $property)
    {
        $this->setProperty($property);
        $this->setName($property->getName());
    }

    /**
     * @param \ReflectionProperty $property
     */
    public function setProperty(\ReflectionProperty $property)
    {
        $this->property = $property;
        $this->parseDocblock($property->getDocComment());
    }

    /**
     * @return \ReflectionProperty
     */
    public function getProperty()
    {
        return $this->property;
    }

    /**
     * @param string $class
     */
    public function setClass($class)
    {
        $this->classname = $class;
    }

    /**
     * @return string
     */
    protected function getId()
    {
        return sprintf(
            '%s::%s',
            $this->getProperty()->getDeclaringClass()->getName(),
            $this->getName()
        );
    }

    /**
     * @param string $docblock
     * @throws \LogicException
     */
    protected function parseDocblock($docblock)
    {
        preg_match_all(
            '/@(\w+)\h+(.+)\s?/',
            $docblock,
            $annotations,
            PREG_SET_ORDER
        );

        foreach ($annotations as $annotation) {
            $this->annotations[$annotation[1]] = $annotation[2];
        }

        if (isset($this->annotations['var'])) {
            $this->setType($this->annotations['var']);
        } else {
            throw new \LogicException(sprintf(
                'Docblock missing or property type not defined: %s::$%s',
                $this->getProperty()->getDeclaringClass()->getName(),
                $this->getProperty()->getName()
            ));
        }

        if (isset($this->annotations['reference'])) {
            $this->referenceClass = $this->annotations['reference'];
        }

        if (strpos($this->getType(), '\\') !== false) {
            $this->classname = str_replace('[]', '', $this->getType());
        } else {
            $this->classname = null;
        }

        preg_match_all(
            '/@alias\h+(\w+)/',
            $docblock,
            $annotationAlias
        );
        $this->aliases = $annotationAlias[1];
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        if (isset($this->validTypes[$type])) {
            $this->type = $this->validTypes[$type];
        } elseif (strpos($type, '\\') === 0) {
            $this->type = $type;
        } else {
            throw new \LogicException(
                sprintf(
                    'Unknown type or class: "%s" in \%s',
                    $type,
                    $this->getProperty()->getDeclaringClass()->getName()
                )
            );
        }
    }

    public function getSchema()
    {
        if ($this->getClass()) {
            $subclass = $this->getClass();
            $subDocument = new $subclass;

            if ($subDocument instanceof Date) {
                return 'ISODate';
            } elseif ($subDocument instanceof \MongoId) {
                return 'ObjectId';
            } else {
                if ($subDocument instanceof LazyDocument) {
                    if ($this->isCollection()) {
                        return [$subDocument->getSchema()];
                    }
                    return $subDocument->getSchema();
                }
            }
        }
        return $this->getType();
    }

    public function isValidValue(&$value)
    {
        if (is_null($value)) {
            return true;
        }

        switch (gettype($value)) {

            case 'array':

                if ($this->isCollection()) {
                    return true;
                }

                $class = $this->getClass();
                $parents = class_parents($class);

                if (in_array('Mongo\Subdocument', $parents)) {
                    return true;
                }
                return false;

            case 'object':

                $class = $this->getClass();

                if ($class === '\Mongo\Field\GeoJson' && $value instanceof GeoAbstract) {
                    return true;
                }

                $parents = class_parents($class);

                if ($value instanceof \DateTime || $value instanceof \MongoDate) {
                    if (trim($class, '\\') === 'Mongo\Field\Date') {
                        return true;
                    }
                    if (in_array('Mongo\Field\Date', $parents)) {
                        return true;
                    }
                }
                return $class instanceof $value;

            default:
                return gettype($value) === $this->getType();
        }
    }

    /**
     * @return string
     */
    public function getClass()
    {
        return $this->classname;
    }

    /**
     * @return string
     */
    protected function getCollectionClass()
    {

        if (!$this->isCollection()) {
            throw new \LogicException(sprintf(
                'This field [%s:%s] is not a collection',
                $this->getProperty()->getDeclaringClass()->getName(),
                $this->getProperty()->getName()
            ));
        }
        if ($this->classname) {
            $classname =  $this->classname . 'Collection';
        } else {
            $classname = '\\Mongo\\Field\\ArrayField';
        }
        if (!class_exists($classname)) {
            $classname = '\\Mongo\\Field\\CollectionField';
        }
        return $classname;
    }

    /***
     * @param mixed $data
     * @return \MongoId|LazyDocument
     */
    public function getInstance($data = null)
    {
        if ($data instanceof \MongoId) {
            return $data;
        }
        $classname = $this->getClass();
        $instance = new $classname;
        if ($data) {
            $instance->setData($data);
        }
        return $instance;
    }

    /***
     * @param array $data
     * @return CollectionField
     */
    public function getCollectionInstance(array $data = null)
    {
        $classname = $this->getCollectionClass();
        $instance = new $classname($this);
        $instance->setData($data);
        return $instance;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return boolean
     */
    public function isCollection()
    {
        return ($this->getType() === 'array' || strpos($this->getType(), '[]') !== false);
    }
    /**
     * @return boolean
     */
    public function isClass()
    {
        return isset($this->classname);
    }

    /**
     * @return array
     */
    public function getAliases()
    {
        return $this->aliases;
    }

    /**
     * @return bool
     */
    public function isReference()
    {
        return !empty($this->referenceClass);
    }

    /**
     * @return string
     */
    public function getReferenceClass()
    {
        return $this->referenceClass;
    }

    /**
     * @param mixed $defaultValue
     */
    public function setDefaultValue($defaultValue)
    {
        $this->defaultValue = $defaultValue;
    }

    /**
     * @return mixed
     */
    public function getDefaultValue()
    {
        return $this->defaultValue;
    }

    /**
     * @param string $type
     * @return string|null
     */
    public function getAnnotation($type)
    {
        return isset($this->annotations[$type]) ? $this->annotations[$type] : null;
    }
}
