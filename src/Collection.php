<?php
/**
 * @author Fabian Fülling <fabian@fabfuel.de>
 * @created 30.01.14
 */

namespace Mongo;

use OC\Util\ArrayHelper;

abstract class Collection
{
    /**
     * @var Db
     */
    protected $database;

    /**
     * @var \MongoCollection
     */
    protected $collection;

    /**
     * @var array
     */
    protected static $identityMap = [];

    /**
     * Return the name of the collection in MongoDB
     *
     * @return string
     */
    abstract public function getCollectionName();

    /**
     * Return the full qualified class name of a document
     *
     * @return string
     */
    abstract public function getDocumentClassName();

    /**
     * @param Db $database
     */
    public function __construct(Db $database)
    {
        $this->setDb($database);
    }

    /**
     * @param string $method
     * @param array $arguments
     * @return mixed
     */
    public function __call($method, $arguments)
    {
        return call_user_func_array(
            array($this->getCollection(), $method),
            $arguments
        );
    }

    /**
     * @param Document $document
     * @param array $options
     * @return array|bool
     */
    public function save(Document $document, $options = [])
    {
        if (!$document->isModified()) {
            return array(
                'notModified' => true,
                'n' => 1,
                'connectionId' => null,
                'err' => null,
                'ok' => 1
            );
        } elseif ($document->isPartial()) {
            return $this->savePartial($document, $options);
        } else {
            return $this->saveFull($document, $options);
        }
    }

    /**
     * @param Document $document
     * @param array $options
     * @return array|bool
     */
    protected function saveFull(Document $document, $options = [])
    {
        $data = $document->toArray();

        $profilerToken = $this->getDb()->startProfiler($this->getProfilerName('save'), ['document' => $data, 'options' => $options]);
        $save = $this->getCollection()->save($data, $options);
        $this->getDb()->stopProfiler($profilerToken);

        if (isset($data['_id'])) {
            $document->setId($data['_id']);
        }

        $document->setData($data);
        $document->setPersistant();

        return $save;
    }

    /**
     * @param Document $document
     * @param array $options
     * @return bool
     */
    protected function savePartial(Document $document, $options = [])
    {
        $criteria = ['_id' => $document->getId()];
        $data = ['$set' => ArrayHelper::toDotNotation($document->getModified())];

        $profilerToken = $this->getDb()->startProfiler($this->getProfilerName('update'), ['document' => $data, 'options' => $options]);
        $update = $this->getCollection()->update($criteria, $data, $options);
        $this->getDb()->stopProfiler($profilerToken);

        $document->setData($data);
        $document->setPersistant();

        return $update;
    }

    /**
     * @param array $query
     * @param array $fields
     * @return Cursor
     */
    public function find(array $query = [], array $fields = [])
    {
        $profilerToken = $this->getDb()->startProfiler($this->getProfilerName('find'), ['query' => $query, 'fields' => $fields]);
        $cursor = new Cursor($this, $query, $fields);
        $this->getDb()->stopProfiler($profilerToken);

        return $cursor;
    }

    /**
     * @param array $query The query criteria to search for.
     * @param array $update The update criteria.
     * @param array $fields Optionally only return these fields.
     * @param array $options An array of options to apply, such as remove the match document from the DB and return it.
     * @return Document|null Returns the original document, or the modified document when new is set.
     */
    public function findAndModify(array $query, array $update = null, array $fields = null, array $options = null)
    {
        $profilerToken = $this->getDb()->startProfiler($this->getProfilerName('findAndModify'), ['query' => $query, 'update' => $update, 'fields' => $fields, 'options' => $options]);
        $document = $this->getCollection()->findAndModify($query, $update, $fields, $options);
        $this->getDb()->stopProfiler($profilerToken);
        $isPartial = !empty($fields);
        return $this->getDocument($document, $isPartial, false);
    }

    /**
     * @param mixed $documentId
     * @param array $fields
     * @return Document|null
     */
    public function findById($documentId, array $fields = [])
    {
        if (ctype_alnum($documentId) && strlen($documentId) === 24) {
            $documentId = new \MongoId($documentId);
        }
        return $this->findOne(array('_id' => $documentId), $fields);
    }

    /**
     * @param array $query
     * @param array $fields
     * @return Document
     */
    public function findOne(array $query = [], array $fields = [])
    {
        $profilerToken = $this->getDb()->startProfiler($this->getProfilerName('findOne'), ['query' => $query, 'fields' => $fields]);
        $result = $this->getCollection()->findOne($query, $fields);
        $this->getDb()->stopProfiler($profilerToken);
        $isPartial = !empty($fields);
        return $this->getDocument($result, $isPartial);
    }

    /**
     * @param array $pipeline
     * @param array $options
     * @return array
     */
    public function aggregate(array $pipeline = [], array $options = [])
    {
        $profilerToken = $this->getDb()->startProfiler($this->getProfilerName('aggregate'), ['pipeline' => $pipeline, 'options' => $options]);
        if ($options) {
            $aggregation = $this->getCollection()->aggregate($pipeline, $options);
        } else {
            $aggregation = $this->getCollection()->aggregate($pipeline);
        }
        $this->getDb()->stopProfiler($profilerToken);
        return $aggregation;
    }

    /**
     * @param array $data
     * @param bool $isPartial
     * @param bool $useIdentityMap
     * @return Document|null
     */
    public function getDocument(array $data = null, $isPartial = false, $useIdentityMap = true)
    {
        if (!isset($data['_id'])) {
            return null;
        }
        $documentId = $data['_id'];

        if ($useIdentityMap === true && $this->isOnIdentityMap($documentId)) {
            return $this->getFromIdentityMap($documentId);
        }

        $className = $this->getDocumentClassName();

        $instance = new $className;
        $instance->fromPersistance($data, $isPartial);
        if ($useIdentityMap) {
            $this->addToIdentityMap($documentId, $instance);
        }
        return $instance;
    }

    /**
     * @param \Mongo\Db $database
     */
    public function setDb(Db $database)
    {
        $this->database = $database;
    }

    /**
     * @return \Mongo\Db
     */
    public function getDb()
    {
        return $this->database;
    }

    /**
     * @param \MongoCollection $collection
     */
    public function setCollection(\MongoCollection $collection)
    {
        $this->collection = $collection;
    }

    /**
     * @return \MongoCollection
     */
    public function getCollection()
    {
        if (is_null($this->collection)) {
            $this->collection = new \MongoCollection(
                $this->getDb()->getDb(),
                $this->getCollectionName()
            );
        }
        return $this->collection;
    }

    /**
     * @param mixed $documentId
     * @param Document $document
     */
    public function addToIdentityMap($documentId, Document $document)
    {
        static::$identityMap[sha1($documentId)] = $document;
    }

    /**
     * @param mixed $documentId
     * @return Document|null
     */
    public function getFromIdentityMap($documentId)
    {
        return isset(static::$identityMap[sha1($documentId)]) ?
            static::$identityMap[sha1($documentId)] : null;
    }

    /**
     * @param mixed $documentId
     * @return bool
     */
    public function isOnIdentityMap($documentId)
    {
        return isset(static::$identityMap[sha1($documentId)]);
    }

    /**
     * @param string $method
     * @return string
     */
    public function getProfilerName($method)
    {
        return sprintf('%s::%s', get_called_class(), $method);
    }
}
