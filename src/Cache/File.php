<?php
/**
 * @author @fabfuel <fabian@fabfuel.de>
 * @created 16.02.14, 07:59
 */
namespace Mongo\Cache;

class File extends Memory implements CacheInterface
{
    /**
     * @var string
     */
    protected $cachePath;

    /**
     * @var bool
     */
    protected $modified = false;

    public function __construct($cachePath)
    {
        $this->setCachePath($cachePath);
        $this->unserialize();
    }

    public function __destruct()
    {
        $this->serialize();
    }

    /**
     * Serialize cache and write to cache path
     * @return bool
     */
    public function serialize()
    {
        if ($this->isModified()) {
            $serialized = serialize($this->cache);
            file_put_contents($this->cachePath, $serialized);
            return true;
        }
        return false;
    }

    /**
     * Serialize cache and write to cache path
     * @return bool
     */
    public function unserialize()
    {
        if (file_exists($this->cachePath)) {
            $cache = file_get_contents($this->cachePath);
            $this->cache = unserialize($cache);
            return true;
        }
        return false;
    }

    /**
     * @param string $key
     * @param mixed $value
     * @return mixed
     */
    public function set($key, $value)
    {
        parent::set($key, $value);
        $this->modified = true;
    }

    /**
     * @param string $cachePath
     */
    public function setCachePath($cachePath)
    {
        $this->cachePath = $cachePath;
    }

    /**
     * @return string
     */
    public function getCachePath()
    {
        return $this->cachePath;
    }

    /**
     * @return boolean
     */
    public function isModified()
    {
        return $this->modified;
    }
}
