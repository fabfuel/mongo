<?php
/**
 * @author @fabfuel <fabian@fabfuel.de>
 * @created 16.02.14, 07:59
 */
namespace Mongo\Cache;

class Apc implements CacheInterface
{
    /**
     * @var int
     */
    protected $ttl = 3600;

    /**
     * @param int $ttl in seconds
     * @throws \RuntimeException
     */
    public function __construct($ttl = null)
    {
        if (!extension_loaded('apc')) {
            // @codeCoverageIgnoreStart
            throw new \RuntimeException('APC extension not loaded');
            // @codeCoverageIgnoreEnd
        }
        if ($ttl) {
            $this->setTtl($ttl);
        }
    }

    /**
     * @param string $key
     * @param mixed $value
     * @param int $ttl
     * @return mixed
     */
    public function set($key, $value, $ttl = null)
    {
        apc_store($key, $value, $ttl ? : $this->getTtl());
    }

    /**
     * @param string $key
     * @return mixed
     */
    public function get($key)
    {
        return apc_fetch($key);
    }

    /**
     * @param string $key
     * @return mixed
     */
    public function exists($key)
    {
        return apc_exists($key);
    }

    /**
     * @param string $key
     * @return mixed
     */
    public function remove($key)
    {
        return apc_delete($key);
    }

    /**
     * @param int $ttl in seconds
     */
    public function setTtl($ttl)
    {
        $this->ttl = $ttl;
    }

    /**
     * @return int in seconds
     */
    public function getTtl()
    {
        return $this->ttl;
    }
}
