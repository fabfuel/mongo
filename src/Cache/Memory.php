<?php
/**
 * @author @fabfuel <fabian@fabfuel.de>
 * @created 16.02.14, 07:59
 */
namespace Mongo\Cache;

class Memory implements CacheInterface
{
    /**
     * @var array
     */
    protected $cache = [];

    /**
     * @param string $key
     * @param mixed $value
     * @return mixed
     */
    public function set($key, $value)
    {
        $this->cache[$key] = $value;
    }

    /**
     * @param string $key
     * @return mixed
     */
    public function get($key)
    {
        return isset($this->cache[$key]) ? $this->cache[$key] : null;
    }

    /**
     * @param string $key
     * @return boolean
     */
    public function exists($key)
    {
        return isset($this->cache[$key]);
    }

    /**
     * @param string $key
     * @return boolean
     */
    public function remove($key)
    {
        if (isset($this->cache[$key])) {
            unset($this->cache[$key]);
            return true;
        }
        return false;
    }
}
