<?php
/**
 * @author @fabfuel <fabian@fabfuel.de>
 * @created 16.02.14, 07:52 
 */
namespace Mongo\Cache;

interface CacheInterface
{
    /**
     * @param string $key
     * @param mixed $value
     * @return mixed
     */
    public function set($key, $value);

    /**
     * @param string $key
     * @return mixed
     */
    public function get($key);

    /**
     * @param string $key
     * @return mixed
     */
    public function exists($key);

    /**
     * @param string $key
     * @return mixed
     */
    public function remove($key);
}
