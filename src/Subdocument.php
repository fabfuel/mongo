<?php
/**
 * @author Fabian Fülling <fabian@fabfuel.de>
 * @created 30.01.14
 */

namespace Mongo;

class Subdocument extends LazyDocument
{
    /**
     * @var Field
     */
    protected $field;

    /**
     * @return  \Mongo\Field
     */
    public function getField()
    {
        return $this->field;
    }

    /**
     * @param \Mongo\Field $field
     */
    public function setField(Field $field)
    {
        $this->field = $field;
    }
}
