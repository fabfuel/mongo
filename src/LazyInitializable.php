<?php
/**
 * @author Fabian Fülling <fabian@fabfuel.de>
 * @created 02.02.14, 09:19 
 */
namespace Mongo;

interface LazyInitializable
{
    /**
     * Load the given data and store in buffer for later initialization
     *
     * @param mixed $data
     * @return void
     */
    public function setData($data);

    /**
     * Convert instance to array
     *
     * @return array
     */
    public function toArray();

    /**
     * Initialize lazy instance and e.g. map data into properties
     *
     * @return bool
     */
    public function initialize();
}
