<?php
/**
 * @author Fabian Fülling <fabian@fabfuel.de>
 * @created 30.01.14
 */

namespace Mongo;

use OC\Util\ArrayHelper;

class Document extends LazyDocument
{
    /**
     * @var bool
     */
    protected $persistant = false;


    /**
     * @var bool
     */
    protected $partial = false;

    /**
     * @param array $data
     * @param bool $partial
     */
    public function fromPersistance(array $data, $partial = false)
    {
        $this->setPartial($partial);
        $this->setData($data);
        $this->setPersistant();
    }

    /**
     * @return boolean
     */
    public function isPersistant()
    {
        return $this->persistant;
    }

    /**
     * Mark document as persistant
     */
    public function setPersistant()
    {
        $this->persistant = true;
    }

    /**
     * @return array
     */
    public function getModified()
    {
        if (!$this->isPersistant()) {
            return $this->toArray();
        } elseif (!$this->isInitialized()) {
            return [];
        }
        return ArrayHelper::diffRecursive($this->toArray(), $this->getData(), true);
    }

    /**
     * @return bool
     */
    public function isModified()
    {
        return count($this->getModified()) > 0;
    }

    /**
     * @param boolean $partial
     */
    public function setPartial($partial)
    {
        $this->partial = $partial;
    }

    /**
     * @return boolean
     */
    public function isPartial()
    {
        return $this->partial;
    }
}
