<?php
/**
 * @author @fabfuel <fabian@fabfuel.de>
 * @created 02.03.14, 12:59 
 */
namespace Mongo\Profiler;

interface ProfilerInterface
{
    /**
     * Start a new benchmark
     *
     * @param string $name Unique identifier like e.g. Class::Method (\Foobar\MyClass::doSomething)
     * @param array $metadata Addtional interesting metadata for this benchmark
     * @return string identifier token
     */
    public function start($name, array $metadata = []);

    /**
     * Stop a running benchmark
     *
     * @param string $token benchmark identifier
     * @return void
     */
    public function stop($token);
}
