<?php
/**
 * @author Fabian Fülling <fabian@fabfuel.de>
 * @created 30.01.14
 */

namespace Mongo;

class Cursor implements \Iterator
{
    /**
     * @var \MongoCursor
     */
    protected $cursor;

    /**
     * @var array
     */
    protected $query = [];

    /**
     * @var array
     */
    protected $fields = [];

    /**
     * @var Collection
     */
    protected $collection;

    /**
     * @param Collection $collection
     * @param array $query
     * @param array $fields
     */
    public function __construct(Collection $collection, array $query = [], array $fields = [])
    {
        $this->setCollection($collection);
        $this->setQuery($query);
        $this->setFields($fields);
    }


    /**
     * @param string $method
     * @param array $arguments
     * @return \MongoCursor|mixed
     */
    public function __call($method, $arguments)
    {
        $result = call_user_func_array(
            array($this->getCursor(), $method),
            $arguments
        );

        if ($result instanceof \MongoCursor) {
            return $this;
        }
        return $result;
    }


    /**
     * @return string
     */
    public function getNamespace()
    {
        return sprintf(
            '%s.%s',
            $this->getCollection()->getDb()->getName(),
            $this->getCollection()->getCollectionName()
        );
    }

    /**
     * @return Document
     */
    public function getNext()
    {
        $this->next();
        return $this->current();
    }

    /**
     * @return Document
     */
    public function current()
    {
        $current = $this->getCursor()->current();
        $isPartial = !empty($this->fields);
        return $this->getCollection()->getDocument($current, $isPartial, false);
    }

    /**
     * @return void Any returned value is ignored.
     */
    public function next()
    {
        $this->getCursor()->next();
    }

    /**
     * @return string
     */
    public function key()
    {
        return $this->getCursor()->key();
    }

    /**
     * @return bool
     */
    public function valid()
    {
        return $this->getCursor()->valid();
    }

    /**
     * @return void Any returned value is ignored.
     */
    public function rewind()
    {
        if ($this->getCursor()->count()) {
            $profilerToken = $this->getCollection()->getDb()->startProfiler(
                $this->getCollection()->getProfilerName('cursor'),
                ['query' => $this->getQuery(), 'fields' => $this->getFields(), 'results' => $this->getCursor()->count()]
            );
            $this->getCursor()->rewind();
            $this->getCollection()->getDb()->stopProfiler($profilerToken);
        }
    }

    /**
     * @param \Mongo\Collection $collection
     */
    public function setCollection($collection)
    {
        $this->collection = $collection;
    }

    /**
     * @return \Mongo\Collection
     */
    public function getCollection()
    {
        return $this->collection;
    }

    /**
     * @param array $fields
     */
    public function setFields($fields)
    {
        $this->fields = $fields;
    }

    /**
     * @return array
     */
    public function getFields()
    {
        return $this->fields;
    }

    /**
     * @param array $query
     */
    public function setQuery($query)
    {
        $this->query = $query;
    }

    /**
     * @return array
     */
    public function getQuery()
    {
        return $this->query;
    }

    /**
     * @param \MongoCursor $cursor
     */
    public function setCursor(\MongoCursor $cursor)
    {
        $this->cursor = $cursor;
    }

    /**
     * @return \MongoCursor
     */
    public function getCursor()
    {
        if (is_null($this->cursor)) {
            $this->cursor = new \MongoCursor(
                $this->getCollection()->getDb()->getClient(),
                $this->getNamespace(),
                $this->getQuery(),
                $this->getFields()
            );
        }
        return $this->cursor;
    }
}
