<?php
/**
 * @author @fabfuel <fabian@fabfuel.de>
 * @created 16.05.14 14:10
 */

namespace Mongo\Field\GeoJson;

abstract class GeoAbstract implements GeoObject
{
    /**
     * @param bool $longitudeFirstValue
     * @return array|null
     */
    public function toArray($longitudeFirstValue = true)
    {
        return [
            'type' => $this->getType(),
            'coordinates' => $this->getCoordinates($longitudeFirstValue)
        ];
    }
}
