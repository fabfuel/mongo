<?php
/**
 * @author @fabfuel <fabian@fabfuel.de>
 * @created 14.05.14 11:15
 */

namespace Mongo\Field\GeoJson;

class LineString extends GeoAbstract implements GeoObject
{
    /**
     * @var Point
     */
    protected $start;

    /**
     * @var Point
     */
    protected $end;

    /**
     * @param array $coordinates Array of arrays with coordinates
     * @param bool $longitudeFirstValue true = [longitude, latitude], false = [latitude, longitude]
     */
    public function __construct(array $coordinates = null, $longitudeFirstValue = true)
    {
        if (!is_null($coordinates)) {
            $this->setCoordinates($coordinates, $longitudeFirstValue);
        }
    }

    /**
     * @param array $coordinates Array of arrays with coordinates
     * @param bool $longitudeFirstValue true = [longitude, latitude], false = [latitude, longitude]
     */
    public function setCoordinates(array $coordinates, $longitudeFirstValue = true)
    {
        if (!isset($coordinates[0]) || !isset($coordinates[1])) {
            throw new \InvalidArgumentException('Array must contain two elements: start and end coordinates');
        }
        $coordinates = array_values($coordinates);
        $this->setStart(new Point($coordinates[0], $longitudeFirstValue));
        $this->setEnd(new Point($coordinates[1], $longitudeFirstValue));
    }

    /**
     * @return Point
     */
    public function getEnd()
    {
        return $this->end;
    }

    /**
     * @param Point $end
     */
    public function setEnd(Point $end)
    {
        $this->end = $end;
    }

    /**
     * @return Point
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * @param Point $start
     */
    public function setStart(Point $start)
    {
        $this->start = $start;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return sprintf(
            '%s - %s',
            $this->getStart(),
            $this->getEnd()
        );
    }

    /**
     * @param bool $longitudeFirstValue true = [longitude, latitude], false = [latitude, longitude]
     * @return array
     */
    public function getCoordinates($longitudeFirstValue = true)
    {
        return [
            $this->getStart()->getCoordinates($longitudeFirstValue),
            $this->getEnd()->getCoordinates($longitudeFirstValue),
        ];
    }

    /**
     * @return string
     */
    public function getType()
    {
        return 'LineString';
    }
}
