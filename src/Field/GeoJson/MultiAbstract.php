<?php
/**
 * @author @fabfuel <fabian@fabfuel.de>
 * @created 16.05.14 12:08
 */

namespace Mongo\Field\GeoJson;

abstract class MultiAbstract extends GeoAbstract implements GeoObject
{
    /**
     * @var GeoObject[]
     */
    protected $geoObjects = [];

    /**
     * Returns the class name of a single geo object which is hold in the collection
     *
     * @return string
     */
    abstract protected function getGeoObjectClassName();

    /**
     * @param array $coordinates
     */
    public function __construct(array $coordinates = null)
    {
        if (!is_null($coordinates)) {
            $this->setCoordinates($coordinates);
        }
    }

    /**
     * @param GeoObject $geoObject
     */
    public function add(GeoObject $geoObject)
    {
        $this->geoObjects[] = $geoObject;
    }

    /**
     * @param array $coordinates
     * @param bool $longitudeFirstValue true = [longitude, latitude], false = [latitude, longitude]
     */
    public function setCoordinates(array $coordinates, $longitudeFirstValue = true)
    {
        $this->geoObjects = [];
        foreach ($coordinates as $coordinate) {
            $geoObjectClass = $this->getGeoObjectClassName();
            $geoObject = new $geoObjectClass($coordinate, $longitudeFirstValue);
            $this->add($geoObject);
        }
    }

    /**
     * @param bool $longitudeFirstValue true = [longitude, latitude], false = [latitude, longitude]
     * @return array
     */
    public function getCoordinates($longitudeFirstValue = true)
    {
        $coords = [];
        foreach ($this->geoObjects as $geoObject) {
            $coords[] = $geoObject->getCoordinates($longitudeFirstValue);
        }
        return $coords;
    }
}
