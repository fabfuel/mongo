<?php
/**
 * @author @fabfuel <fabian@fabfuel.de>
 * @created 16.05.14 12:15
 */

namespace Mongo\Field\GeoJson;

class MultiPoint extends MultiAbstract
{
    /**
     * @return string
     */
    public function getType()
    {
        return 'MultiPoint';
    }

    /**
     * @return string
     */
    protected function getGeoObjectClassName()
    {
        return 'Mongo\Field\GeoJson\Point';
    }
}
