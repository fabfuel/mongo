<?php
/**
 * @author @fabfuel <fabian@fabfuel.de>
 * @created 14.05.14 11:21
 */

namespace Mongo\Field\GeoJson;

class Polygon extends GeoAbstract implements GeoObject, \Iterator
{
    /**
     * @var Point[]
     */
    protected $points = [];

    /**
     * @var int
     */
    protected $index = 0;

    /**
     * @param array $coordinates Array of arrays with coordinates
     * @param bool $longitudeFirstValue true = [longitude, latitude], false = [latitude, longitude]
     */
    public function __construct(array $coordinates = null, $longitudeFirstValue = true)
    {
        if ($coordinates) {
            $this->setCoordinates($coordinates, $longitudeFirstValue);
        }
    }

    /**
     * @param array $coordinates
     * @param bool $longitudeFirstValue true = [longitude, latitude], false = [latitude, longitude]
     */
    public function setCoordinates(array $coordinates, $longitudeFirstValue = true)
    {
        $firstElement = current(current($coordinates));
        if (is_array($firstElement)) {
            $outerRing = current($coordinates);
        } else {
            $outerRing = $coordinates;
        }

        $this->points = [];
        foreach ($outerRing as $pointCoords) {
            $point = new Point($pointCoords, $longitudeFirstValue);
            $this->add($point);
        }
        $this->ensureLastEqualsFirst();
    }

    /**
     * @return string
     */
    public function getType()
    {
        return 'Polygon';
    }

    /**
     * @param bool $longitudeFirstValue true = [longitude, latitude], false = [latitude, longitude]
     * @return array
     */
    public function getCoordinates($longitudeFirstValue = true)
    {
        $coords = [];
        foreach ($this as $point) {
            $coords[] = $point->getCoordinates($longitudeFirstValue);
        }
        return [$coords];
    }


    /**
     * @param Point $point
     * @return $this
     */
    public function add(Point $point)
    {
        $this->points[] = $point;
        return $this;
    }

    /**
     * @return Point
     */
    public function current()
    {
        return $this->points[$this->index];
    }

    /**
     * @return int
     */
    public function key()
    {
        return $this->index;
    }

    /**
     * @return bool
     */
    public function valid()
    {
        return isset($this->points[$this->index]);
    }

    public function next()
    {
        $this->index += 1;
    }

    public function rewind()
    {
        $this->index = 0;
        $this->ensureLastEqualsFirst();
    }

    protected function ensureLastEqualsFirst()
    {
        if ($this->getFirst() != $this->getLast()) {
            $this->add($this->getFirst());
        }
    }

    public function getFirst()
    {
        return isset($this->points[0]) ? $this->points[0] : null;
    }

    public function getLast()
    {
        $lastIndex = count($this->points)-1;
        return isset($this->points[$lastIndex]) ? $this->points[$lastIndex] : null;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        $string = '';
        foreach ($this as $point) {
            $string .= $point . PHP_EOL;
        }
        return $string;
    }
}
