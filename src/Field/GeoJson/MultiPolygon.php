<?php
/**
 * @author @fabfuel <fabian@fabfuel.de>
 * @created 16.05.14 12:15
 */

namespace Mongo\Field\GeoJson;

class MultiPolygon extends MultiAbstract
{
    /**
     * @return string
     */
    public function getType()
    {
        return 'MultiPolygon';
    }

    /**
     * @return string
     */
    protected function getGeoObjectClassName()
    {
        return 'Mongo\Field\GeoJson\Polygon';
    }
}
