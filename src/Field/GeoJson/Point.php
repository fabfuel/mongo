<?php
/**
 * @author @fabfuel <fabian@fabfuel.de>
 * @created 14.05.14 11:09
 */

namespace Mongo\Field\GeoJson;

class Point extends GeoAbstract implements GeoObject
{
    /**
     * @var float
     */
    protected $lng = 0.0;

    /**
     * @var float
     */
    protected $lat = 0.0;

    /**
     * @param array $coordinates Array of arrays with coordinates
     * @param bool $longitudeFirstValue true = [longitude, latitude], false = [latitude, longitude]
     */
    public function __construct(array $coordinates = null, $longitudeFirstValue = true)
    {
        if (!is_null($coordinates)) {
            $this->setCoordinates($coordinates, $longitudeFirstValue);
        }
    }

    /**
     * @return float
     */
    public function getLat()
    {
        return $this->lat;
    }

    /**
     * @param float $lat
     */
    public function setLat($lat)
    {
        $this->lat = $lat;
    }

    /**
     * @return float
     */
    public function getLng()
    {
        return $this->lng;
    }

    /**
     * @param float $lng
     */
    public function setLng($lng)
    {
        $this->lng = $lng;
    }

    /**
     * @param bool $longitudeFirstValue true = [longitude, latitude], false = [latitude, longitude]
     * @return double[]
     */
    public function getCoordinates($longitudeFirstValue = true)
    {
        if ($longitudeFirstValue) {
            return [
                (float)$this->getLng(),
                (float)$this->getLat()
            ];
        }
        return [
            (float)$this->getLat(),
            (float)$this->getLng()
        ];
    }

    /**
     * @param array $coordinates Array of arrays with coordinates
     * @param bool $longitudeFirstValue true = [longitude, latitude], false = [latitude, longitude]
     */
    public function setCoordinates(array $coordinates, $longitudeFirstValue = true)
    {
        if ($longitudeFirstValue === true) {
            $this->setLng($coordinates[0]);
            $this->setLat($coordinates[1]);
        } else {
            $this->setLng($coordinates[1]);
            $this->setLat($coordinates[0]);
        }
    }

    /**
     * @return string
     */
    public function getType()
    {
        return 'Point';
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return sprintf(
            '%s:%s',
            $this->getLng(),
            $this->getLat()
        );
    }
}
