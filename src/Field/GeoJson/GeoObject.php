<?php
/**
 * @author @fabfuel <fabian@fabfuel.de>
 * @created 16.05.14 12:10
 */

namespace Mongo\Field\GeoJson;

interface GeoObject
{
    /**
     * @return string
     */
    public function getType();

    /**
     * @param bool $longitudeFirstValue true = [longitude, latitude], false = [latitude, longitude]
     * @return array
     */
    public function getCoordinates($longitudeFirstValue = true);

    /**
     * @param array $coordinates
     * @param bool $longitudeFirstValue true = [longitude, latitude], false = [latitude, longitude]
     */
    public function setCoordinates(array $coordinates, $longitudeFirstValue = true);
}
