<?php
/**
 * @author Fabian Fülling <fabian@fabfuel.de>
 * @created 02.02.14, 09:02
 */

namespace Mongo\Field;

use Mongo\LazyAbstract;
use Mongo\LazyInitializable;

/**
 * Class Date
 * @package Mongo\Field
 * @method static format (string $format)
 * @method static add (\DateInterval $interval)
 * @method static modify (string $modify)
 * @method static setDate (int $year, int $month, int $day)
 * @method static setISODate (int $year, int $week, int $day = 1)
 * @method static setTime (int $hour, int $minute, int $second = 0)
 * @method static setTimestamp (int $unixtimestamp)
 * @method static setTimezone (\DateTimeZone $timezone)
 * @method static sub (\DateInterval $interval)
 * @method \DateInterval diff (\DateTime $dateTime2)
 */
class Date extends LazyAbstract implements LazyInitializable
{
    /**
     * @var \DateTime
     */
    protected $dateTime;

    public function setData($data)
    {
        if ($data instanceof \DateTime) {
            $this->setDateTime($data);
        } else {
            parent::setData($data);
        }
    }
    
    /**
     * @return bool
     */
    public function initialize()
    {
        if ($this->isInitialized()) {
            return true;
        }
        $this->setInitialized();
        if ($this->data) {
            $this->getDateTime(true)->setTimestamp($this->getMongoTimestamp());
        }
        return true;
    }

    /**
     * @return \MongoDate
     */
    public function toArray()
    {
        if ($this->isModified()) {
            return new \MongoDate($this->getDateTime()->getTimestamp());
        }
        return $this->data;
    }

    /**
     * @return bool
     */
    public function isModified()
    {
        return $this->getDateTime() && $this->getDateTime()->getTimestamp() !== $this->getMongoTimestamp();
    }

    /**
     * Get the MongoDate timestamp
     *
     * @return int|null
     */
    protected function getMongoTimestamp()
    {
        return (isset($this->data->sec)) ? $this->data->sec : null;
    }

    /**
     * Delegate calls to \DateTime instance
     *
     * @param string $method
     * @param array $args
     * @return mixed
     */
    public function __call($method, array $args)
    {
        return call_user_func_array(
            array($this->getDateTime(true), $method),
            $args
        );
    }

    /**
     * @param string $format
     * @return string
     */
    public function format($format)
    {
        if (!$this->getDateTime()) {
            return '';
        }
        return $this->getDateTime(true)->format($format);
    }

    /**
     * @param bool $createOnDemand
     * @return \DateTime
     */
    public function getDateTime($createOnDemand = false)
    {
        if ($createOnDemand && is_null($this->dateTime)) {
            $this->dateTime = new \DateTime('12:00');
        }
        return $this->dateTime;
    }

    /**
     * @param \DateTime $dateTime
     */
    public function setDateTime($dateTime)
    {
        $this->dateTime = $dateTime;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->format(\DateTime::RSS) ?: '';
    }
}
