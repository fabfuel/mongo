<?php
/**
 * @author @fabfuel <fabian@fabfuel.de>
 * @created 02.02.14, 15:51 
 */

namespace Mongo\Field;

use Mongo\LazyAbstract;
use Mongo\LazyInitializable;

class ArrayField extends LazyAbstract implements LazyInitializable, \Countable, \Iterator, \ArrayAccess
{
    /**
     * @var int
     */
    protected $offset = 0;

    /**
     * @param array $data
     */
    public function setData($data)
    {
        if (is_null($data)) {
            $data = [];
        }
        parent::setData($data);
    }

    public function initialize()
    {
    }

    /**
     * Add an element to the array
     *
     * @param mixed $element
     */
    public function add($element)
    {
        $this->data[] = $element;
    }

    /**
     * Add an element, if it does NOT already exist in the array (see MongoDB's $addToSet)
     *
     * @param mixed $element
     * @param bool $strict (strict comparison "===")
     * @return bool
     */
    public function addToSet($element, $strict = false)
    {
        if (!in_array($element, $this->data, $strict)) {
            $this->data[] = $element;
            return true;
        }
        return false;
    }

    /**
     * @param mixed $element
     * @param bool $strict (strict comparison "===")
     * @return bool
     */
    public function remove($element, $strict = false)
    {
        if (in_array($element, $this->data, $strict)) {
            unset($this->data[array_search($element, $this->data, $strict)]);
            $this->data = array_values($this->data);
            return true;
        }
        return false;
    }

    /**
     * @param mixed $element
     * @param bool $strict (strict comparison "===")
     * @return bool
     */
    public function has($element, $strict = false)
    {
        return in_array($element, $this->data, $strict);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return $this->data;
    }

    /**
     * @return int
     */
    public function count()
    {
        return count($this->data);
    }

    public function current()
    {
        return $this->data[$this->offset];
    }

    public function next()
    {
        $this->offset += 1;
    }

    public function key()
    {
        return $this->offset;

    }

    public function valid()
    {
        return isset($this->data[$this->offset]);
    }

    public function rewind()
    {
        $this->offset = 0;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return implode(', ', $this->toArray());
    }

    /**
     * @param mixed $offset
     * @return bool
     */
    public function offsetExists($offset)
    {
        return isset($this->data[$offset]);
    }

    /**
     * @param mixed $offset
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return $this->data[$offset];
    }

    /**
     * @param mixed $offset
     * @param mixed $value
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        $this->data[$offset] = $value;
    }

    /**
     * @param mixed $offset
     */
    public function offsetUnset($offset)
    {
        unset($this->data[$offset]);
    }
}
