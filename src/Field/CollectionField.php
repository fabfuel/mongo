<?php
/**
 * @author @fabfuel <fabian@fabfuel.de>
 * @created 02.02.14, 15:51 
 */

namespace Mongo\Field;

use Mongo\LazyAbstract;
use Mongo\LazyInitializable;
use Mongo\Subdocument;
use Mongo\Field;

class CollectionField extends LazyAbstract implements LazyInitializable, \Countable, \Iterator
{
    /**
     * @var Subdocument[]
     */
    protected $subdocuments = [];

    /**
     * @var Subdocument[]
     */
    protected $idMap = [];

    /**
     * @var Field
     */
    protected $field;

    /**
     * @var int
     */
    protected $offset = 0;

    /**
     * @param \Mongo\Field $field
     */
    public function __construct(Field $field)
    {
        $this->setField($field);
    }

    /**
     * @return  \Mongo\Field
     */
    public function getField()
    {
        return $this->field;
    }

    /**
     * @param \Mongo\Field $field
     */
    public function setField(Field $field)
    {
        $this->field = $field;
    }

    /**
     * @return bool|void
     */
    public function initialize()
    {
        if ($this->initialized) {
            return;
        }
        $this->initialized = true;
        foreach ($this->data as $subdocumentData) {
            $subdocument = $this->getField()->getInstance();
            $subdocument->setData($subdocumentData);
            $this->add($subdocument);
        }
    }

    /**
     * @param array $data
     */
    public function setData($data)
    {
        if (is_null($data)) {
            $data = [];
        }
        parent::setData($data);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        if (!$this->initialized) {
            return $this->data;
        }
        $subdocuments = [];
        foreach ($this->subdocuments as $subdocument) {
            if (!$subdocument->getId()) {
                $subdocument->setId(uniqid());
            }
            $data = $subdocument->toArray();

            /**
             * Only return subdocuments that have data and not just an id
             */
            if (array_diff_key($data, ['_id' => 1])) {
                $subdocuments[] = $data;
            }
        }
        return $subdocuments;
    }

    /**
     * @param Subdocument $subdocument
     */
    public function add(Subdocument $subdocument)
    {
        $this->subdocuments[] = $subdocument;
        $this->idMap[$subdocument->getId()] = $subdocument;
    }

    /**
     * Get a subdocument by its id
     *
     * @param string $subdocumentId
     * @return Subdocument|null
     */
    public function getById($subdocumentId)
    {
        if (isset($this->idMap[$subdocumentId])) {
            $this->idMap[$subdocumentId]->initialize();
            return $this->idMap[$subdocumentId];
        }
        return null;
    }

    /**
     * @param integer $index
     * @return Subdocument
     */
    public function getAt($index)
    {
        if (isset($this->subdocuments[$index])) {
            $subdocument = $this->subdocuments[$index];
        } else {
            $subdocument = $this->getField()->getInstance();
            $this->add($subdocument);
        }
        $subdocument->initialize();
        return $subdocument;
    }

    /**
     * @return Subdocument
     */
    public function getFirst()
    {
        return $this->getAt(0);
    }

    /**
     * @param Subdocument $subdocument
     * @return bool
     */
    public function remove(Subdocument $subdocument)
    {
        $index = array_search($subdocument, $this->subdocuments, true);
        return $this->removeAt($index);
    }

    /**
     * @param int $index
     * @return bool
     */
    public function removeAt($index)
    {
        if (isset($this->subdocuments[$index])) {
            unset($this->subdocuments[$index]);
            $this->subdocuments = array_merge($this->subdocuments);
            return true;
        }
        return false;
    }

    /**
     * @return int
     */
    public function count()
    {
        return count($this->subdocuments);
    }

    public function current()
    {
        $subdocument = $this->subdocuments[$this->offset];
        $subdocument->initialize();
        return $subdocument;
    }

    public function next()
    {
        $this->offset += 1;
    }

    public function key()
    {
        return $this->offset;
    }

    public function valid()
    {
        return isset($this->subdocuments[$this->offset]);
    }

    public function rewind()
    {
        $this->offset = 0;
    }
}
