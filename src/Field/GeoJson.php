<?php
/**
 * @author @fabfuel <fabian@fabfuel.de>
 * @created 16.05.14 12:07
 */

namespace Mongo\Field;

use Mongo\Field\GeoJson\GeoAbstract;
use Mongo\Field\GeoJson\GeoObject;
use Mongo\Field\GeoJson\LineString;
use Mongo\Field\GeoJson\MultiLineString;
use Mongo\Field\GeoJson\MultiPoint;
use Mongo\Field\GeoJson\MultiPolygon;
use Mongo\Field\GeoJson\Point;
use Mongo\Field\GeoJson\Polygon;
use Mongo\LazyAbstract;
use Mongo\LazyInitializable;

class GeoJson extends LazyAbstract implements GeoObject, LazyInitializable
{
    const TYPE_POINT = 'Point';
    const TYPE_POLYGON = 'Polygon';
    const TYPE_LINESTRING = 'LineString';
    const TYPE_MULTIPOINT = 'MultiPoint';
    const TYPE_MULTIPOLYGON = 'MultiPolygon';
    const TYPE_MULTILINESTRING = 'MultiLineString';

    /**
     * @var GeoAbstract
     */
    protected $geoObject;

    /**
     * @var bool $longitudeFirstValue true = [longitude, latitude], false = [latitude, longitude]
     */
    protected $longitudeFirstValue = true;

    /**
     * @param array|GeoObject $data
     */
    public function setData($data)
    {
        if ($data instanceof GeoObject) {
            $this->data['type'] = $data->getType();
            $this->data['coordinates'] = $data->getCoordinates();
        } else {
            $this->data = $data;
        }
    }

    /**
     * Returns a GeoJSON array
     *
     * @return array
     */
    public function toArray()
    {
        if ($this->isInitialized() && $this->getGeoObject()) {
            return $this->getGeoObject()->toArray($this->isLongitudeFirstValue());
        }
        return $this->data;
    }

    /**
     * Initialize lazy instance and e.g. map data into properties
     *
     * @return bool
     */
    public function initialize()
    {
        if ($this->isInitialized()) {
            return true;
        }

        $this->setInitialized();

        if (isset($this->data['type']) && isset($this->data['coordinates'])) {
            $geoObjectClass = __CLASS__ . '\\' . $this->data['type'];
            $geoObject = new $geoObjectClass($this->data['coordinates']);
            $this->setGeoObject($geoObject);
        }

        return true;
    }

    /**
     * @return GeoAbstract
     */
    public function getGeoObject()
    {
        return $this->geoObject;
    }

    /**
     * @param GeoAbstract $geoObject
     */
    public function setGeoObject(GeoAbstract $geoObject)
    {
        $this->geoObject = $geoObject;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getGeoObject()->__toString();
    }

    /**
     * @param string $type
     */
    protected function validateType($type)
    {
        if ($this->getGeoObject() && $this->getGeoObject()->getType() !== $type) {
            throw new \RuntimeException(sprintf(
                'GeoJson already initialized as type "%s" and change of type not confirmed',
                $this->getGeoObject()->getType()
            ));
        }
    }

    /**
     * @param bool $allowTypeChange Allow change of GeoJson type
     * @return Polygon
     */
    public function getPolygon($allowTypeChange = false)
    {
        $allowTypeChange || $this->validateType(self::TYPE_POLYGON);

        if (is_null($this->geoObject) || $this->getGeoObject()->getType() !== self::TYPE_POLYGON) {
            $this->geoObject = new Polygon();
        }
        return $this->geoObject;
    }

    /**
     * @param bool $allowTypeChange Allow change of GeoJson type
     * @return Point
     */
    public function getPoint($allowTypeChange = false)
    {
        $allowTypeChange || $this->validateType(self::TYPE_POINT);

        if (is_null($this->geoObject) || $this->getGeoObject()->getType() !== self::TYPE_POINT) {
            $this->geoObject = new Point();
        }
        return $this->geoObject;
    }

    /**
     * @param bool $allowTypeChange Allow change of GeoJson type
     * @return LineString
     */
    public function getLineString($allowTypeChange = false)
    {
        $allowTypeChange || $this->validateType(self::TYPE_LINESTRING);

        if (is_null($this->geoObject) || $this->getGeoObject()->getType() !== self::TYPE_LINESTRING) {
            $this->geoObject = new LineString();
        }
        return $this->geoObject;
    }

    /**
     * @param bool $allowTypeChange Allow change of GeoJson type
     * @return MultiPolygon
     */
    public function getMultiPolygon($allowTypeChange = false)
    {
        $allowTypeChange || $this->validateType(self::TYPE_MULTIPOLYGON);

        if (is_null($this->geoObject) || $this->getGeoObject()->getType() !== self::TYPE_MULTIPOLYGON) {
            $this->geoObject = new MultiPolygon();
        }
        return $this->geoObject;
    }

    /**
     * @param bool $allowTypeChange Allow change of GeoJson type
     * @return MultiPoint
     */
    public function getMultiPoint($allowTypeChange = false)
    {
        $allowTypeChange || $this->validateType(self::TYPE_MULTIPOINT);

        if (is_null($this->geoObject) || $this->getGeoObject()->getType() !== self::TYPE_MULTIPOINT) {
            $this->geoObject = new MultiPoint();
        }
        return $this->geoObject;
    }

    /**
     * @param bool $allowTypeChange Allow change of GeoJson type
     * @return MultiLineString
     */
    public function getMultiLineString($allowTypeChange = false)
    {
        $allowTypeChange || $this->validateType(self::TYPE_MULTILINESTRING);

        if (is_null($this->geoObject) || $this->getGeoObject()->getType() !== self::TYPE_MULTILINESTRING) {
            $this->geoObject = new MultiLineString();
        }
        return $this->geoObject;
    }

    /**
     * @return boolean
     */
    public function isLongitudeFirstValue()
    {
        return $this->longitudeFirstValue;
    }

    /**
     * @param boolean $longitudeFirstValue
     */
    public function setLongitudeFirstValue($longitudeFirstValue)
    {
        $this->longitudeFirstValue = $longitudeFirstValue;
    }

    /**
     * @return string
     */
    public function getType()
    {
        if (!$this->getGeoObject()) {
            throw new \RuntimeException('GeoObject has not been initialized!');
        }
        return $this->getGeoObject()->getType();
    }

    /**
     * @param bool $longitudeFirstValue true = [longitude, latitude], false = [latitude, longitude]
     * @return array
     */
    public function getCoordinates($longitudeFirstValue = true)
    {
        if (!$this->getGeoObject()) {
            throw new \RuntimeException('GeoObject has not been initialized!');
        }
        return $this->getGeoObject()->getCoordinates($longitudeFirstValue);
    }

    /**
     * @param array $coordinates
     * @param bool $longitudeFirstValue true = [longitude, latitude], false = [latitude, longitude]
     */
    public function setCoordinates(array $coordinates, $longitudeFirstValue = true)
    {
        if (!$this->getGeoObject()) {
            throw new \RuntimeException('GeoObject has not been initialized!');
        }
        return $this->getGeoObject()->setCoordinates($coordinates, $longitudeFirstValue);
    }
}
