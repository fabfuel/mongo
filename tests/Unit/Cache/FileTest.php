<?php
/**
 * @author @fabfuel <fabian@fabfuel.de>
 * @created 12.07.14, 13:42 
 */
namespace Mongo\Test\Unit\Cache;

use Mongo\Cache\File;

class FileTest extends \PHPUnit_Framework_TestCase
{
    const CACHE_PATH = '/tmp/mongo.cache';

    /**
     * @var File
     */
    protected $cache;

    /**
     * @covers Mongo\Cache\File::__construct
     */
    public function setUp()
    {
        $this->cache = new File(__DIR__ . '/file.cache');
    }

    /**
     * @covers Mongo\Cache\File::setCachePath
     * @covers Mongo\Cache\File::getCachePath
     */
    public function testSetGetCachePath()
    {
        $testPath = '/some/path/to/cache';
        $this->cache->setCachePath($testPath);
        $this->assertSame($testPath, $this->cache->getCachePath());
    }

    /**
     * @covers Mongo\Cache\File::serialize
     * @covers Mongo\Cache\File::__destruct
     */
    public function testSerialize()
    {
        $testPath = '/tmp/mongo.cache';

        $this->cache->setCachePath($testPath);
        $this->assertFalse($this->cache->serialize());

        $this->cache->set('lorem', 'ipsum');
        $this->assertTrue($this->cache->isModified());

        $this->assertTrue($this->cache->serialize());

        $this->assertFileExists($testPath);
        $this->assertFileEquals($testPath, __DIR__ . '/file.cache');

        unset($this->cache);
    }

    /**
     * @covers Mongo\Cache\File::unserialize
     */
    public function testUnserialize()
    {
        $cache['lorem'] = 'ipsum';

        $this->assertTrue($this->cache->unserialize());
    }

    /**
     * @covers Mongo\Cache\File::unserialize
     */
    public function testUnserializeWithInvalidPath()
    {
        $this->cache->setCachePath(__DIR__ . '/file.cache.invalid');
        $this->assertFalse($this->cache->unserialize());
    }

    /**
     * @covers Mongo\Cache\File::get
     * @covers Mongo\Cache\File::set
     * @covers Mongo\Cache\File::isModified
     * @covers Mongo\Cache\File::remove
     * @covers Mongo\Cache\File::exists
     */
    public function testGetAndSet()
    {
        $this->assertFalse($this->cache->isModified());

        $this->assertFalse($this->cache->exists('foobar'));
        $this->assertNull($this->cache->get('foobar'));

        $foobar = 'foobar';

        $this->cache->set('foobar', $foobar);
        $this->assertSame($foobar, $this->cache->get('foobar'));
        $this->assertTrue($this->cache->exists('foobar'));

        $remove = $this->cache->remove('foobar');
        $this->assertTrue($remove);

        $this->assertFalse($this->cache->exists('foobar'));
        $this->assertNull($this->cache->get('foobar'));

        $remove = $this->cache->remove('foobar');
        $this->assertFalse($remove);

        $this->assertTrue($this->cache->isModified());
    }
}
