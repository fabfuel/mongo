<?php
/**
 * @author @fabfuel <fabian@fabfuel.de>
 * @created 12.07.14, 13:42 
 */
namespace Mongo\Test\Unit\Cache;

use Mongo\Cache\Memory;

class MemoryTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var Memory
     */
    protected $cache;

    public function setUp()
    {
        $this->cache = new Memory();

    }

    public function testSetandGet()
    {
        $this->assertFalse($this->cache->exists('foobar'));
        $this->assertNull($this->cache->get('foobar'));

        $foobar = 'foobar';

        $this->cache->set('foobar', $foobar);
        $this->assertSame($foobar, $this->cache->get('foobar'));
        $this->assertTrue($this->cache->exists('foobar'));

        $remove = $this->cache->remove('foobar');
        $this->assertTrue($remove);

        $this->assertFalse($this->cache->exists('foobar'));
        $this->assertNull($this->cache->get('foobar'));

        $remove = $this->cache->remove('foobar');
        $this->assertFalse($remove);
    }
}
