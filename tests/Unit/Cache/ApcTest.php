<?php
/**
 * @author @fabfuel <fabian@fabfuel.de>
 * @created 12.07.14, 13:42 
 */
namespace Mongo\Test\Unit\Cache;

use Mongo\Cache\Apc;

class ApcTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var Apc
     */
    protected $cache;

    public function setUp()
    {
        if (!ini_get('apc.enable_cli')) {
            $this->markTestSkipped('APC on CLI disabled');
        }

        $this->cache = new Apc(10);
    }

    public function testSetandGet()
    {
        $this->assertFalse($this->cache->exists('foobar'));
        $this->assertFalse($this->cache->get('foobar'));

        $foobar = 'foobar';

        $this->cache->set('foobar', $foobar);
        $this->assertSame($foobar, $this->cache->get('foobar'));
        $this->assertTrue($this->cache->exists('foobar'));

        $remove = $this->cache->remove('foobar');
        $this->assertTrue($remove);

        $this->assertFalse($this->cache->exists('foobar'));
        $this->assertFalse($this->cache->get('foobar'));

        $remove = $this->cache->remove('foobar');
        $this->assertFalse($remove);
    }
}
