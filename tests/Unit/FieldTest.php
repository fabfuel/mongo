<?php
/**
 * @author @fabfuel <fabian@fabfuel.de>
 * @created 07.02.14, 08:06 
 */
namespace Mongo\Test\Unit;

class FieldTest extends \PHPUnit_Framework_TestCase 
{
    /**
     * @var \Mongo\Subdocument
     */
    protected $subdocument;

    public function setUp()
    {
        $this->subdocument = $this->getMockForAbstractClass(
            '\Mongo\Subdocument',
            array(),
            '',
            false
        );
    }

    public function testSetGetField()
    {
        $fieldMock = $this->getMockBuilder('\Mongo\Field')
            ->disableOriginalConstructor()
            ->getMock();

        $this->subdocument->setField($fieldMock);
        $this->assertSame($fieldMock, $this->subdocument->getField());
    }
}
