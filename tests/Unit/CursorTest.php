<?php
/**
 * @author @fabfuel <fabian@fabfuel.de>
 * @created 08.02.14, 07:23
 */
namespace Mongo\Test\Unit;

use Mongo\Cursor;

class CursorTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @covers \Mongo\Cursor::__construct
     */
    public function testConstruct()
    {
        $collectionMock = $this->getCollectionMock();
        $query = ['foo'=>'bar', 'lorem'=>'ipsum'];
        $fields = ['foo', 'bar', 'lorem', 'ipsum'];

        $cursor = $this->getMockBuilder('\Mongo\Cursor')
            ->disableOriginalConstructor()
            ->setMethods(array('setCollection', 'setQuery', 'setFields'))
            ->getMockForAbstractClass();

        $cursor
            ->expects($this->once())
            ->method('setCollection')
            ->with($collectionMock);

        $cursor
            ->expects($this->once())
            ->method('setQuery')
            ->with($query);

        $cursor
            ->expects($this->once())
            ->method('setFields')
            ->with($fields);

        $cursor->__construct($collectionMock , $query, $fields);
    }


    /**
     * @covers \Mongo\Cursor::__call
     */
    public function testCall()
    {
        $foobarArgs = [1,2,3,4,5];
        $foobarReturn = ['my', 'foobar', 'return'];
        $loremArgs = ['lorem', 'ipsum'];
        $loremReturn = ['my', 'lorem', 'return'];
        $boolArgs = true;

        $mongoCursorMock = $this->getMockBuilder('\MongoCursor')
            ->disableOriginalConstructor()
            ->setMethods(array('callA', 'callB', 'callC'))
            ->getMock();

        $collectionMock = $this->getCollectionMock();

        $cursor = new Cursor($collectionMock);
        $cursor->setCursor($mongoCursorMock);

        $mongoCursorMock
            ->expects($this->once())
            ->method('callA')
            ->will($this->returnValue($foobarReturn));

        $mongoCursorMock
            ->expects($this->once())
            ->method('callB')
            ->with($loremArgs)
            ->will($this->returnValue($loremReturn));

        $mongoCursorMock
            ->expects($this->once())
            ->method('callC')
            ->with($boolArgs)
            ->will($this->returnValue($mongoCursorMock));

        $this->assertEquals($foobarReturn, $cursor->callA($foobarArgs));
        $this->assertEquals($loremReturn, $cursor->callB($loremArgs));
        $this->assertEquals($cursor, $cursor->callC($boolArgs));
    }


    /**
     * @covers \Mongo\Cursor::getNamespace
     */
    public function testGetNamespace()
    {
        $collectionMock = $this->getCollectionMock(1, 0, 1, 1);
        $cursor = new Cursor($collectionMock);
        $namespace = $cursor->getNamespace();
        $this->assertSame('lorem.ipsum', $namespace);
    }

    /**
     * @covers \Mongo\Cursor::getCollection
     * @covers \Mongo\Cursor::setCollection
     */
    public function testSetAndGetCollection()
    {
        $collectionMock = $this->getCollectionMock();
        $cursor = new Cursor($collectionMock);

        $this->assertEquals($collectionMock, $cursor->getCollection());

        $anotherCollectionMock = $this->getMockBuilder('\Mongo\Collection')
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();

        $cursor->setCollection($anotherCollectionMock);
        $this->assertSame($anotherCollectionMock, $cursor->getCollection());
    }

    /**
     * @covers \Mongo\Cursor::getFields
     * @covers \Mongo\Cursor::setFields
     */
    public function testSetAndGetFields()
    {
        $collectionMock = $this->getCollectionMock();
        $cursor = new Cursor($collectionMock);

        $this->assertEquals([], $cursor->getFields());

        $fields = ['foo'=>'bar', 'lorem'=>'ipsum'];

        $cursor->setFields($fields);
        $this->assertSame($fields, $cursor->getFields());
    }

    /**
     * @covers \Mongo\Cursor::getQuery
     * @covers \Mongo\Cursor::setQuery
     */
    public function testSetAndGetQuery()
    {
        $collectionMock = $this->getCollectionMock();
        $cursor = new Cursor($collectionMock);

        $this->assertEquals([], $cursor->getQuery());

        $query = ['foo'=>'bar', 'lorem'=>'ipsum'];

        $cursor->setQuery($query);
        $this->assertSame($query, $cursor->getQuery());
    }

    protected function getCollectionMock($callsGetName = 0, $callsGetClient = 0, $callsGetCollectionName = 0, $callsGetDb = 0, $methods = ['getDb', 'getCollectionName'])
    {
        $databaseMock = $this->getMockBuilder('\Mongo\Db')
            ->disableOriginalConstructor()
            ->setMethods(array('getName', 'getClient'))
            ->getMock();

        $databaseMock
            ->expects($this->exactly($callsGetName))
            ->method('getName')
            ->will($this->returnValue('lorem'));

        $databaseMock
            ->expects($this->exactly($callsGetClient))
            ->method('getClient')
            ->will($this->returnValue(new \MongoClient(null, array('connect' => false))));

        $collectionMock = $this->getMockBuilder('\Mongo\Collection')
            ->setConstructorArgs(array($databaseMock, 'lorem'))
            ->setMethods($methods)
            ->getMockForAbstractClass();

        $collectionMock
            ->expects($this->exactly($callsGetCollectionName))
            ->method('getCollectionName')
            ->will($this->returnValue('ipsum'));

        $collectionMock
            ->expects($this->exactly($callsGetDb))
            ->method('getDb')
            ->will($this->returnValue($databaseMock));

        return $collectionMock;
    }

    /**
     * @covers \Mongo\Cursor::key
     */
    public function testKey()
    {
        $mongoCursorMock = $this->getMockBuilder('\MongoCursor')
            ->disableOriginalConstructor()
            ->setMethods(array('key'))
            ->getMock();

        $collectionMock = $this->getCollectionMock();

        $cursor = new Cursor($collectionMock);
        $cursor->setCursor($mongoCursorMock);

        $mongoCursorMock
            ->expects($this->once())
            ->method('key')
            ->will($this->returnValue('foobar'));

        $this->assertSame('foobar', $cursor->key());
    }

    /**
     * @covers \Mongo\Cursor::valid
     */
    public function testValid()
    {
        $mongoCursorMock = $this->getMockBuilder('\MongoCursor')
            ->disableOriginalConstructor()
            ->setMethods(array('valid'))
            ->getMock();

        $collectionMock = $this->getCollectionMock();

        $cursor = new Cursor($collectionMock);
        $cursor->setCursor($mongoCursorMock);

        $mongoCursorMock
            ->expects($this->once())
            ->method('valid')
            ->will($this->returnValue(true));

        $this->assertSame(true, $cursor->valid());
    }

    /**
     * @covers \Mongo\Cursor::rewind
     */
    public function testRewind()
    {
        $mongoCursorMock = $this->getMockBuilder('\MongoCursor')
            ->disableOriginalConstructor()
            ->setMethods(array('rewind', 'count'))
            ->getMock();

        $collectionMock = $this->getCollectionMock(0, 0, 0, 2);

        $cursor = new Cursor($collectionMock);
        $cursor->setCursor($mongoCursorMock);

        $mongoCursorMock
            ->expects($this->exactly(2))
            ->method('count')
            ->will($this->returnValue(5));

        $mongoCursorMock
            ->expects($this->once())
            ->method('rewind');

        $cursor->rewind();
    }

    /**
     * @covers \Mongo\Cursor::rewind
     */
    public function testRewindWithoutResults()
    {
        $mongoCursorMock = $this->getMockBuilder('\MongoCursor')
            ->disableOriginalConstructor()
            ->setMethods(array('rewind', 'count'))
            ->getMock();

        $collectionMock = $this->getCollectionMock();

        $cursor = new Cursor($collectionMock);
        $cursor->setCursor($mongoCursorMock);

        $mongoCursorMock
            ->expects($this->once())
            ->method('count')
            ->will($this->returnValue(0));

        $mongoCursorMock
            ->expects($this->never())
            ->method('rewind');

        $cursor->rewind();
    }

    /**
     * @covers \Mongo\Cursor::next
     */
    public function testNext()
    {
        $mongoCursorMock = $this->getMockBuilder('\MongoCursor')
            ->disableOriginalConstructor()
            ->setMethods(array('next'))
            ->getMock();

        $collectionMock = $this->getCollectionMock();

        $cursor = new Cursor($collectionMock);
        $cursor->setCursor($mongoCursorMock);

        $mongoCursorMock
            ->expects($this->once())
            ->method('next');

        $cursor->next();
    }

    /**
     * @covers \Mongo\Cursor::getCursor
     * @covers \Mongo\Cursor::setCursor
     */
    public function testGetSetCursor()
    {
        $collectionMock = $this->getCollectionMock();

        $mongoCursorMock = $this->getMockBuilder('\MongoCursor')
            ->disableOriginalConstructor()
            ->getMock();

        $cursor = new Cursor($collectionMock);

        $cursor->setCursor($mongoCursorMock);
        $this->assertSame($mongoCursorMock, $cursor->getCursor());
    }


    /**
     * @covers \Mongo\Cursor::getCursor
     */
    public function testGetCursorWhenEmpty()
    {
        $collectionMock = $this->getCollectionMock(1, 1, 1, 2);
        $query = ['foo'=>'bar', 'lorem'=>'ipsum'];
        $fields = ['foo', 'bar', 'lorem', 'ipsum'];

        $cursor = new Cursor($collectionMock, $query, $fields);
        $this->assertInstanceOf('\MongoCursor', $cursor->getCursor());
    }

    /**
     * @covers \Mongo\Cursor::current
     */
    public function testCurrent()
    {
        $currentData = ['foo' => 'bar'];
        $document = ['my', 'document'];

        $collectionMock = $this->getCollectionMock(0, 0, 0, 0, ['getDocument']);

        $collectionMock
            ->expects($this->once())
            ->method('getDocument')
            ->with($currentData, false)
            ->will($this->returnValue($document));

        $mongoCursorMock = $this->getMockBuilder('\MongoCursor')
            ->disableOriginalConstructor()
            ->setMethods(array('current'))
            ->getMock();

        $mongoCursorMock
            ->expects($this->once())
            ->method('current')
            ->will($this->returnValue($currentData));


        $cursor = new Cursor($collectionMock);
        $cursor->setCursor($mongoCursorMock);

        $this->assertEquals($document, $cursor->current());
    }

    /**
     * @covers \Mongo\Cursor::current
     */
    public function testCurrentWithFieldsDefined()
    {
        $currentData = ['foo' => 'bar'];
        $document = ['my', 'document'];

        $collectionMock = $this->getCollectionMock(0, 0, 0, 0, ['getDocument']);

        $collectionMock
            ->expects($this->once())
            ->method('getDocument')
            ->with($currentData, true)
            ->will($this->returnValue($document));

        $mongoCursorMock = $this->getMockBuilder('\MongoCursor')
            ->disableOriginalConstructor()
            ->setMethods(array('current'))
            ->getMock();

        $mongoCursorMock
            ->expects($this->once())
            ->method('current')
            ->will($this->returnValue($currentData));


        $cursor = new Cursor($collectionMock, ['some', 'query'], ['my', 'fields']);
        $cursor->setCursor($mongoCursorMock);

        $this->assertEquals($document, $cursor->current());
    }

    /**
     * @covers \Mongo\Cursor::getNext
     */
    public function testGetNext()
    {
        $currentData = ['foo' => 'bar'];
        $document = ['my', 'document'];

        $collectionMock = $this->getCollectionMock(0, 0, 0, 0, ['getDocument']);

        $collectionMock
            ->expects($this->once())
            ->method('getDocument')
            ->with($currentData, false)
            ->will($this->returnValue($document));

        $mongoCursorMock = $this->getMockBuilder('\MongoCursor')
            ->disableOriginalConstructor()
            ->setMethods(array('current', 'next'))
            ->getMock();

        $mongoCursorMock
            ->expects($this->once())
            ->method('current')
            ->will($this->returnValue($currentData));

        $mongoCursorMock
            ->expects($this->once())
            ->method('next');

        $cursor = new Cursor($collectionMock);
        $cursor->setCursor($mongoCursorMock);

        $this->assertEquals($document, $cursor->getNext());

    }
}
