<?php
/**
 * @author Fabian Fülling <fabian@fabfuel.de>
 * @created 05.02.14
 */
namespace Mongo\Test\Unit;

use Mongo\Client;
use Mongo\Db;

class CollectionTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var \Mongo\Collection
     */
    protected $collection;

    public function setUp()
    {
        $this->collection = $this->getMockForAbstractClass(
            '\Mongo\Collection',
            array(),
            '',
            false
        );
    }

    /**
     * @covers \Mongo\Collection::getDb
     * @covers \Mongo\Collection::setDb
     */
    public function testGetSetDb()
    {
        $db = $this->getMockBuilder('\Mongo\Db')
            ->disableOriginalConstructor()
            ->getMock();

        $this->collection->setDb($db);
        $this->assertSame($db, $this->collection->getDb());
    }

    /**
     * @covers \Mongo\Collection::getCollection
     * @covers \Mongo\Collection::setCollection
     */
    public function testGetSetCollection()
    {
        $collection = $this->getMockBuilder('\MongoCollection')
            ->disableOriginalConstructor()
            ->getMock();

        $this->collection->setCollection($collection);
        $this->assertSame($collection, $this->collection->getCollection());
    }

    /**
     * @covers \Mongo\Collection::getCollection
     */
    public function testGetCollectionWhenEmpty()
    {
        $databaseName = 'lorem';
        $collectionName = 'ipsum';

        $client = new Client(null, array('connect' => false));
        $database =  new Db($client, $databaseName);

        $collectionMock = $this->getMockForAbstractClass(
            '\Mongo\Collection',
            array(),
            '',
            false,
            true,
            true,
            array(
                'getDb',
                'getCollectionName',
            )
        );

        $collectionMock
            ->expects($this->once())
            ->method('getDb')
            ->will($this->returnValue($database));

        $collectionMock
            ->expects($this->once())
            ->method('getCollectionName')
            ->will($this->returnValue($collectionName));

        /**
         * @var \MongoCollection $collection
         */
        $collection = $collectionMock->getCollection();

        $this->assertInstanceOf('\MongoCollection', $collection);
        $this->assertSame($collectionName, $collection->getName());
    }

    /**
     * @covers \Mongo\Collection::__construct
     */
    public function testConstruct()
    {
        $collection = $this->getMockForAbstractClass(
            '\Mongo\Collection',
            array(),
            '',
            false
        );

        $db = $this->getMockBuilder('\Mongo\Db')
            ->disableOriginalConstructor()
            ->getMock();

        $collection->__construct($db);

        $this->assertSame($db, $collection->getDb());
    }

    /**
     * @covers \Mongo\Collection::__call
     */
    public function testCall()
    {
        $mongoCollectionMock = $this->getMockBuilder('\MongoCollection')
            ->disableOriginalConstructor()
            ->setMethods(array('foobar'))
            ->getMock();

        $mongoCollectionMock
            ->expects($this->once())
            ->method('foobar')
            ->with(12345);

        $collection = $this->getCollectionMock();

        $collection
            ->expects($this->once())
            ->method('getCollection')
            ->will($this->returnValue($mongoCollectionMock));

        $collection->foobar(12345);
    }

    /**
     * @covers \Mongo\Collection::save
     * @covers \Mongo\Collection::saveFull
     */
    public function testSaveNewDocument()
    {
        $testArray = array('foo' => 'bar');

        $documentMock = $this->getMockBuilder('\Mongo\Document')
            ->disableOriginalConstructor()
            ->setMethods(array('isModified', 'toArray', 'setId'))
            ->getMock();

        $documentMock
            ->expects($this->once())
            ->method('isModified')
            ->will($this->returnValue(true));

        $documentMock
            ->expects($this->once())
            ->method('toArray')
            ->will($this->returnValue($testArray));

        $documentMock
            ->expects($this->never())
            ->method('setId');

        $mongoCollectionMock = $this->getMockBuilder('\MongoCollection')
            ->disableOriginalConstructor()
            ->setMethods(array('save'))
            ->getMock();

        $mongoCollectionMock
            ->expects($this->once())
            ->method('save')
            ->with($testArray)
            ->will($this->returnValue(true));

        $collectionMock = $this->getCollectionMock();

        $collectionMock
            ->expects($this->once())
            ->method('getCollection')
            ->will($this->returnValue($mongoCollectionMock));

        $databaseName = 'lorem';
        $client = new Client(null, array('connect' => false));
        $collectionMock->setDb(new Db($client, $databaseName));

        $saveResult = $collectionMock->save($documentMock);
        $this->assertTrue($saveResult);
    }

    /**
     * @covers \Mongo\Collection::save
     * @covers \Mongo\Collection::saveFull
     */
    public function testSaveExistingDocument()
    {
        $documentId = 'XYZ';
        $testArray = array('_id' => $documentId, 'foo' => 'bar');

        $documentMock = $this->getMockBuilder('\Mongo\Document')
            ->disableOriginalConstructor()
            ->getMock();

        $documentMock
            ->expects($this->once())
            ->method('isModified')
            ->will($this->returnValue(true));

        $documentMock
            ->expects($this->once())
            ->method('toArray')
            ->will($this->returnValue($testArray));

        $documentMock
            ->expects($this->once())
            ->method('setPersistant');

        $documentMock
            ->expects($this->once())
            ->method('setData')
            ->with($testArray);

        $documentMock
            ->expects($this->exactly(1))
            ->method('setId')
            ->with($documentId);

        $mongoCollectionMock = $this->getMockBuilder('\MongoCollection')
            ->disableOriginalConstructor()
            ->setMethods(array('save'))
            ->getMock();

        $mongoCollectionMock
            ->expects($this->once())
            ->method('save')
            ->with($testArray)
            ->will($this->returnValue(true));

        $collectionMock = $this->getCollectionMock();

        $collectionMock
            ->expects($this->once())
            ->method('getCollection')
            ->will($this->returnValue($mongoCollectionMock));

        $databaseName = 'lorem';
        $client = new Client(null, array('connect' => false));
        $collectionMock->setDb(new Db($client, $databaseName));

        $saveResult = $collectionMock->save($documentMock);
        $this->assertTrue($saveResult);
    }

    /**
     * @covers \Mongo\Collection::save
     * @covers \Mongo\Collection::savePartial
     */
    public function testSavePartial()
    {
        $testId = 'a12b34c56d';
        $testData = array(
            'foo' => 'bar',
            'lorem' => array(
                'ipsum' => 'dalar',
                'kator' => 'rilast',
            )
        );
        $expectedData = array(
            'foo' => 'bar',
            'lorem.ipsum' => 'dalar',
            'lorem.kator' => 'rilast',
        );
        $testOptions = array('w' => 'majority', 'j' => 1);

        $updateCriteria = array('_id' => $testId);
        $updateData = array('$set' => $expectedData);

        $documentMock = $this->getMockBuilder('\Mongo\Document')
            ->disableOriginalConstructor()
            ->setMethods(array('isModified', 'getModified', 'isPartial', 'getId'))
            ->getMock();

        $documentMock
            ->expects($this->once())
            ->method('getId')
            ->will($this->returnValue($testId));

        $documentMock
            ->expects($this->once())
            ->method('isModified')
            ->will($this->returnValue(true));

        $documentMock
        ->expects($this->once())
            ->method('getModified')
            ->will($this->returnValue($testData));

        $documentMock
            ->expects($this->once())
            ->method('isPartial')
            ->will($this->returnValue(true));

        $mongoCollectionMock = $this->getMockBuilder('\MongoCollection')
            ->disableOriginalConstructor()
            ->setMethods(array('update'))
            ->getMock();

        $mongoCollectionMock
            ->expects($this->once())
            ->method('update')
            ->with($updateCriteria, $updateData, $testOptions);

        $collectionMock = $this->getCollectionMock();

        $collectionMock
            ->expects($this->once())
            ->method('getCollection')
            ->will($this->returnValue($mongoCollectionMock));

        $databaseName = 'lorem';
        $client = new Client(null, array('connect' => false));
        $collectionMock->setDb(new Db($client, $databaseName));

        $collectionMock->save($documentMock, $testOptions);
    }

    /**
     * @covers \Mongo\Collection::find
     */
    public function testFind()
    {
        $query = ['lorem', 'ipsum'];
        $fields = ['foo', 'bar'];
        $collectionMock = $this->getCollectionMock();

        $databaseName = 'lorem';
        $client = new Client(null, array('connect' => false));
        $collectionMock->setDb(new Db($client, $databaseName));

        $cursor = $collectionMock->find($query, $fields);
        $this->assertInstanceOf('\Mongo\Cursor', $cursor);
        $this->assertSame($query, $cursor->getQuery());
        $this->assertSame($fields, $cursor->getFields());
    }

    /**
     * @covers \Mongo\Collection::save
     */
    public function testSaveWithoutModification()
    {
        $documentMock = $this->getMockBuilder('\Mongo\Document')
            ->disableOriginalConstructor()
            ->setMethods(array('isModified'))
            ->getMock();

        $documentMock
            ->expects($this->once())
            ->method('isModified')
            ->will($this->returnValue(false));

        $collection = $this->getCollectionMock();

        $saveResult = $collection->save($documentMock);
        $this->assertArrayHasKey('notModified', $saveResult);
        $this->assertArrayHasKey('ok', $saveResult);
        $this->assertArrayHasKey('err', $saveResult);
    }

    /**
     * @covers       \Mongo\Collection::findById
     * @dataProvider getIdsToFind
     */
    public function testFindById($id)
    {
        $collection = $this->getCollectionMock();

        $collection
            ->expects($this->once())
            ->method('findOne')
            ->with(array('_id' => $id))
            ->will($this->returnValue(true));

        $foundById = $collection->findById($id);
        $this->assertTrue($foundById);
    }

    /**
     * @return array
     */
    public function getIdsToFind()
    {
        return array(
            array(1),
            array(13548465465),
            array('foobar'),
            array('my-id'),
            array(new \MongoId('52ece503c15ec29ab20041a7'))
        );
    }

    /**
     * @covers \Mongo\Collection::getDocument
     */
    public function testGetDocument()
    {
        $collectionMock = $this->getCollectionMock();

        $data = array(
            '_id' => rand(99999, 999999),
            'lorem' => 'ipsum',
            'foo' => 'bar',
        );

        $collectionMock
            ->expects($this->once())
            ->method('getDocumentClassName')
            ->will($this->returnValue('\Mongo\Document'));

        $document = $collectionMock->getDocument($data);

        $this->assertInstanceOf('\Mongo\Document', $document);
        $this->assertSame($document, $collectionMock->getDocument($data));
    }

    /**
     * @covers \Mongo\Collection::getDocument
     */
    public function testGetDocumentWithoutId()
    {
        $data = array(
            'lorem' => 'ipsum',
            'foo' => 'bar',
        );
        $this->assertNull($this->collection->getDocument($data));
    }

    /**
     * @covers \Mongo\Collection::getDocument
     */
    public function testGetDocumentFromIdentityMap()
    {
        $documentId = rand(99999, 999999);
        $documentMock = $this->getMockBuilder('\Mongo\Document')
            ->disableOriginalConstructor()
            ->getMock();

        $data = array(
            '_id' => $documentId
        );

        $this->collection->addToIdentityMap($documentId, $documentMock);

        $this->assertSame($documentMock, $this->collection->getDocument($data));

    }

    /**
     * @covers \Mongo\Collection::findOne
     */
    public function testFindOne()
    {
        $query = array('foo' => 'bar');
        $fields = array('lorem' => true, 'ipsum' => true);

        $mongoCollectionMock = $this->getMockBuilder('\MongoCollection')
            ->disableOriginalConstructor()
            ->setMethods(array('findOne'))
            ->getMock();

        $mongoCollectionMock
            ->expects($this->once())
            ->method('findOne')
            ->with($query, $fields);

        $collectionMock = $this->getMockForAbstractClass(
            '\Mongo\Collection',
            array(),
            '',
            false,
            true,
            true,
            array(
                'getDocument',
            )
        );
        $collectionMock->setCollection($mongoCollectionMock);

        $databaseName = 'lorem';
        $client = new Client(null, array('connect' => false));
        $collectionMock->setDb(new Db($client, $databaseName));

        $document = $collectionMock->findOne($query, $fields);

        $this->assertNull($document);
    }

    /**
     * @covers \Mongo\Collection::findAndModify
     */
    public function testFindAndModify()
    {
        $query = array('foo' => 'bar');
        $update = array('lorem' => 'ipsum');
        $fields = array('lorem' => true, 'ipsum' => true);
        $options = array('multi' => true);


        $mongoCollectionMock = $this->getMockBuilder('\MongoCollection')
            ->disableOriginalConstructor()
            ->setMethods(array('findAndModify'))
            ->getMock();

        $mongoCollectionMock
            ->expects($this->once())
            ->method('findAndModify')
            ->with($query, $update, $fields, $options);

        $collectionMock = $this->getMockForAbstractClass(
            '\Mongo\Collection',
            array(),
            '',
            false,
            true,
            true,
            array(
                'getDocument',
            )
        );
        $collectionMock->setCollection($mongoCollectionMock);

        $databaseName = 'lorem';
        $client = new Client(null, array('connect' => false));
        $collectionMock->setDb(new Db($client, $databaseName));

        $document = $collectionMock->findAndModify($query, $update, $fields, $options);

        $this->assertNull($document);
    }

    /**
     * @covers       \Mongo\Collection::findById
     * @dataProvider getMongoIdStringsToFind
     */
    public function testFindByMongoIdString($id)
    {
        $collection = $this->getCollectionMock();

        $collection
            ->expects($this->once())
            ->method('findOne')
            ->with(array('_id' => $id))
            ->will($this->returnValue(true));

        $foundById = $collection->findById($id);
        $this->assertTrue($foundById);
    }

    /**
     * @return array
     */
    public function getMongoIdStringsToFind()
    {
        return array(
            array('52ece503c15ec29ab20041a7'),
        );
    }

    /**
     * @return \PHPUnit_Framework_MockObject_MockObject
     */
    protected function getCollectionMock()
    {
        return $collection = $this->getMockForAbstractClass(
            '\Mongo\Collection',
            array(),
            '',
            false,
            true,
            true,
            array(
                'getDocumentClassName',
                'getCollection',
                'findOne',
            )
        );
    }

    /**
     * @covers \Mongo\Collection::isOnIdentityMap
     * @covers \Mongo\Collection::addToIdentityMap
     * @covers \Mongo\Collection::getFromIdentityMap
     */
    public function testIdentityMap()
    {
        $documentId = rand(99999, 999999);
        $documentMock = $this->getMockBuilder('\Mongo\Document')
            ->disableOriginalConstructor()
            ->getMock();

        $this->assertFalse($this->collection->isOnIdentityMap($documentId));
        $this->assertNull($this->collection->getFromIdentityMap($documentId));

        $this->collection->addToIdentityMap($documentId, $documentMock);
        $this->assertTrue($this->collection->isOnIdentityMap($documentId));

        $this->assertSame(
            $documentMock,
            $this->collection->getFromIdentityMap($documentId)
        );
        $this->assertSame(
            $documentMock,
            $this->collection->getFromIdentityMap($documentId)
        );
    }

    public function testAggregation()
    {
        $pipeline = [
            '$where' => [
            ]
        ];


        $db = $this->getMockBuilder('\Mongo\Db')
            ->disableOriginalConstructor()
            ->getMock();

        $this->collection->setDb($db);

        $collection = $this->getMockBuilder('\MongoCollection')
            ->disableOriginalConstructor()
            ->getMock();

        $this->collection->setCollection($collection);


        $collection
            ->expects($this->once())
            ->method('aggregate')
            ->with($pipeline)
            ->will($this->returnValue('success'));


        $aggregation = $this->collection->aggregate($pipeline);

        $this->assertSame('success', $aggregation);
    }
}
