<?php
/**
 * @author @fabfuel <fabian@fabfuel.de>
 * @created 31.07.14 10:32
 */
namespace Mongo\Test\Unit;

use Mongo\Test\Example\Document\Order;
use Mongo\Test\Example\Document\Reference;
use Mongo\Test\Example\Document\User;
use Mongo\Test\Functional\TestAbstract;

class LazyDocumentTest extends TestAbstract
{
    /**
     * @var \MongoId
     */
    protected $orderId;

    /**
     * @var \MongoId
     */
    protected $userId;

    /**
     * @covers Mongo\LazyDocument::getReferencedObject
     */
    public function testGetNullReference()
    {
        $order = new Order();
        $this->assertNull($order->getUser());
    }

    /**
     * @covers Mongo\LazyDocument::get
     * @covers Mongo\LazyDocument::getReferencedObject
     */
    public function testSetReference()
    {
        $user = new User();
        $user->setId($this->getUserId());

        $order = new Order();
        $order->setId($this->getOrderId());
        $order->setUser($user);

        $this->assertSame($user, $order->getUser());

        $orderArray = $order->toArray();

        $this->assertSame($this->getOrderId(), $orderArray['_id']);
        $this->assertSame($this->getUserId(), $orderArray['user']);
    }

    /**
     * @covers Mongo\LazyDocument::getReferencedObject
     * @covers Mongo\LazyDocument::getCollection
     */
    public function testSetReferencedObjectId()
    {
        $user = new User();
        $user->setId($this->getUserId());

        $order = new Order();
        $order->setId($this->getOrderId());
        $order->setUser($this->getUserId());

        $collection = $this->getMockBuilder('\Mongo\Collection')
            ->disableOriginalConstructor()
            ->getMock();

        $collection->expects($this->once())
            ->method('findById')
            ->with($this->getUserId())
            ->will($this->returnValue($user));

        $db = $this->getMockBuilder('\Mongo\Db')
            ->disableOriginalConstructor()
            ->getMock();

        $db->expects($this->once())
            ->method('getCollection')
            ->with('\Mongo\Test\Example\Collection\Users')
            ->will($this->returnValue($collection));

        $order->setDatabase($db);

        $this->assertSame($user, $order->getUser());

        $orderArray = $order->toArray();

        $this->assertSame($this->getOrderId(), $orderArray['_id']);
        $this->assertSame($this->getUserId(), $orderArray['user']);
    }

    /**
     * @covers Mongo\LazyDocument::set
     */
    public function testSetMongoId()
    {
        $mongoId = new \MongoId();

        $reference = new Reference();
        $reference->setReferenceId($mongoId);

        $this->assertSame($mongoId, $reference->getReferenceId());
    }

    /**
     * @return \MongoId
     */
    protected function getOrderId()
    {
        if (is_null($this->orderId)) {
            $this->orderId = new \MongoId();
        }
        return $this->orderId;
    }

    /**
     * @return \MongoId
     */
    protected function getUserId()
    {
        if (is_null($this->userId)) {
            $this->userId = new \MongoId();
        }
        return $this->userId;
    }
}
