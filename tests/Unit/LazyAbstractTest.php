<?php
/**
 * @author @fabfuel <fabian@fabfuel.de>
 * @created 07.02.14, 08:06 
 */
namespace Mongo\Test\Unit;

class LazyAbstractTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var \Mongo\LazyAbstract
     */
    protected $lazy;

    public function setUp()
    {
        $this->lazy = $this->getMockForAbstractClass(
            '\Mongo\LazyAbstract',
            array(),
            '',
            false
        );
    }

    public function testHasData()
    {
        $this->assertFalse($this->lazy->hasData());
        $this->lazy->setData(['foo' => 'bar']);
        $this->assertTrue($this->lazy->hasData());
    }

    public function testSetGetData()
    {
        $this->assertSame([], $this->lazy->getData());

        $data = array('foo' => 'bar');

        $this->lazy->setData($data);
        $this->assertSame($data, $this->lazy->getData());
    }

    public function testIsInitialized()
    {
        $this->assertFalse($this->lazy->isInitialized());

        $this->lazy->setInitialized(true);
        $this->assertTrue($this->lazy->isInitialized());

        $this->lazy->setInitialized(false);
        $this->assertFalse($this->lazy->isInitialized());
    }
}
