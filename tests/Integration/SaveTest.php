<?php
/**
 * @author @fabfuel <fabian@fabfuel.de>
 * @created 31.10.14, 16:32 
 */
namespace Mongo\Test\Integration;

use Mongo\Test\Example\Collection\Users;
use Mongo\Test\Example\Document\User;

class SaveTest extends IntegrationTestAbstract
{
    public function testSaveDocument()
    {
        $userData = require dirname(__DIR__) . '/Stubs/user.php';
        $user = new User();
        $user->setData($userData);

        $users = new Users($this->getDb());

        $result = $users->save($user);

        $this->assertSame(1.0, $result['ok']);

        $this->assertSame($userData, $user->toArray());
    }
}
