<?php
/**
 * @author @fabfuel <fabian@fabfuel.de>
 * @created 31.10.14, 16:30 
 */
namespace Mongo\Test\Integration;

use Mongo\Client;
use Mongo\Db;

abstract class IntegrationTestAbstract extends \PHPUnit_Framework_TestCase
{
    /**
     * @var Db
     */
    protected static $db;

    public static function setUpBeforeClass()
    {
        try {
            $client = new Client();
            static::$db = new Db($client, 'mongomappertests');
        } catch (\Exception $excpetion) {
            self::markTestSkipped('No database connection, skipping integration tests');
        }
    }

    /**
     * @return Db
     */
    public static function getDb()
    {
        return self::$db;
    }
}
