<?php
/**
 * @author @fabfuel <fabian@fabfuel.de>
 * @created 28.02.14, 20:16 
 */

namespace Mongo\Test\Functional;

abstract class DbTestAbstract extends \PHPUnit_Framework_TestCase
{
    /**
     * @var array
     */
    protected $user = [];

    public function setUp()
    {
        $this->user = include dirname(__DIR__) . '/Stubs/user.php';
    }

    public function getCollectionMock($collectionClass)
    {
        /**
         * @var \Mongo\Collection $collection
         */
        $collection = new $collectionClass(new \Mongo\Db($this->getMongoClientMock(), 'testing'));
        $collection->setCollection($this->getMongoCollectionMock());

        return $collection;
    }

    public function getMongoClientMock()
    {
        return $this->getMockBuilder('\MongoClient')
            ->setConstructorArgs([
                    "mongodb://localhost:27017",
                    ["connect" => false]
            ])
            ->getMock();
    }

    public function getMongoDbMock()
    {
        return $this->getMockBuilder('\MongoDB')
            ->setConstructorArgs([
                    $this->getMongoClientMock(),
                    'testing'
            ])
            ->getMock();
    }

    public function getMongoCollectionMock()
    {
        return $this->getMockBuilder('\MongoCollection')
            ->setConstructorArgs([
                $this->getMongoDbMock(),
                'testing'
            ])
            ->getMock();

    }
}
