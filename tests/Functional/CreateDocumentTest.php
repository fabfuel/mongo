<?php
/**
 * @author @fabfuel <fabian@fabfuel.de>
 * @created 28.02.14, 06:58 
 */

namespace Mongo\Test\Functional;

use Mongo\Test\Example\Document\User;

class CreateDocumentTest extends TestAbstract
{
    public function testCreateDocument()
    {
        $user = new User();
        $user->getName()->setFirst($this->user['name']['first']);
        $user->getName()->setLast($this->user['name']['last']);
        $user->getName()->setScreen($this->user['name']['screen']);
        $user->getName()->setTitle($this->user['name']['title']);

        $user->getAddress()->setStreet($this->user['address']['street']);
        $user->getAddress()->setZip($this->user['address']['zip']);
        $user->getAddress()->setCity($this->user['address']['city']);
        $user->getAddress()->setState($this->user['address']['state']);
        $user->getAddress()->setCountry($this->user['address']['country']);

        $user->setAlarm(new \DateTime('01.03.2014 06:30'));
        $user->setBirthday(new \DateTime('01.04.2000 14:00'));

        $child = $user->getChild()->getFirst();
        $child
            ->setFirstname('Max')
            ->setAge(7)
            ->setId('12345');

        $child->getGame()->getFirst()
                ->setName('GameBoy')
                ->setType('video game')
                ->setId('12345');

        $child->getGame()->getAt(1)
                ->setName('Monopoly')
                ->setType('parlor game')
                ->setId('12345');

        $this->assertEquals('GameBoy', $child->getGame()->getFirst()->getName());


        $this->assertEquals($this->user, $user->toArray());

        $this->assertEquals(
            $this->user['name']['first'] . ' ' . $this->user['name']['last'],
            $user->getName()->getFormatted()
        );
    }

    public function testCreateDocumentFromArray()
    {
        $user = new User();
        $user->fromArray($this->user);

        $this->assertEquals($this->user, $user->toArray());
    }

    public function testCreateDocumentFromArrayWithDotNotation()
    {
        $dotNotation = include dirname(__DIR__) . '/Stubs/user-dotnotation.php';

        $user = new User();
        $user->fromArray($dotNotation);

        $this->assertEquals($this->user, $user->toArray());
    }
}
