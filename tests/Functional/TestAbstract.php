<?php
/**
 * @author @fabfuel <fabian@fabfuel.de>
 * @created 28.02.14, 20:16 
 */

namespace Mongo\Test\Functional;

abstract class TestAbstract extends \PHPUnit_Framework_TestCase
{
    /**
     * @var array
     */
    protected $user = [];

    public function setUp()
    {
        $this->user = include dirname(__DIR__) . '/Stubs/user.php';
    }
}
