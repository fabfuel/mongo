<?php

namespace Mongo\Test\Functional;

use Mongo\Field\GeoJson;
use Mongo\Test\Example\Document\Provider;

/**
 * @author @fabfuel <fabian@fabfuel.de>
 * @created 28.02.14, 20:04
 */
class GeoJsonTest extends TestAbstract
{
    protected $polygonBerlin = [
        [52.45370801465149, 13.319549560546875],
        [52.44826791583389, 13.338432312011719],
        [52.44784941886125, 13.359375],
        [52.4501511030057, 13.371734619140625],
        [52.45098804923731, 13.38958740234375],
        [52.45245266687292, 13.400917053222656],
        [52.45308034523523, 13.412246704101562],
        [52.457473843286216, 13.427352905273438],
        [52.46333115883462, 13.442115783691406],
        [52.46793278868704, 13.447265625],
        [52.47378871279816, 13.454475402832031],
        [52.48319838709334, 13.4637451171875],
        [52.49239701180821, 13.473358154296875],
        [52.49406928234804, 13.487777709960938],
        [52.493233155027156, 13.502197265625],
        [52.495741489296144, 13.512496948242188],
        [52.50472851217964, 13.512496948242188],
        [52.514758367880944, 13.502540588378906],
        [52.52165256594633, 13.4912109375],
        [52.536899524504044, 13.48297119140625],
        [52.54024061643999, 13.469924926757812],
        [52.55047112887276, 13.482284545898438],
        [52.559864397033024, 13.477134704589844],
        [52.559864397033024, 13.464775085449219],
        [52.558612077463984, 13.438682556152344],
        [52.572802939398834, 13.426666259765625],
        [52.57342890109033, 13.405723571777344],
        [52.572802939398834, 13.392333984375],
        [52.570090335474916, 13.366241455078125],
        [52.57092500070833, 13.345985412597656],
        [52.570090335474916, 13.328475952148438],
        [52.567586244454795, 13.3209228515625],
        [52.561742809358755, 13.317489624023438],
        [52.54608691552761, 13.342552185058594],
        [52.53982299385283, 13.328819274902344],
        [52.549636074382285, 13.316802978515625],
        [52.54796591773237, 13.300323486328125],
        [52.537734821325, 13.293800354003906],
        [52.53126085529297, 13.29345703125],
        [52.52081696319122, 13.297576904296875],
        [52.513295823153165, 13.295173645019531],
        [52.508072051744215, 13.294143676757812],
        [52.50514646853436, 13.294486999511719],
        [52.49657756892365, 13.294830322265625],
        [52.4926060491034, 13.294830322265625],
        [52.48696169336169, 13.302383422851562],
        [52.482152967102564, 13.3099365234375],
        [52.474834331541906, 13.304443359375],
        [52.47232480481694, 13.30169677734375],
        [52.46772363412945, 13.305473327636719],
        [52.46374950869897, 13.308906555175781],
        [52.46103016351592, 13.311653137207031],
        [52.45872904793214, 13.308563232421875],
        [52.453917235802265, 13.3106231689],
    ];
    protected $pointACoords = [13.368810, 52.485571];
    protected $pointBCoords = [13.345631, 52.586905];
    protected $pointCCoords = [13.144992, 52.586768];
    protected $pointDCoords = [13.162901, 52.472778];

    public function testFromPersistance()
    {
        $geoJson = new GeoJson();
        $geoJson->setData($this->getPointAGeometry());
        $this->assertSame($this->getPointAGeometry(), $geoJson->toArray());

        $this->setExpectedException('\RuntimeException');
        $geoJson->getType();

        $this->setExpectedException('\RuntimeException');
    }

    /**
     * @expectedException \RuntimeException
     */
    public function testGetCoordinatesWhenNotInitialized()
    {
        $geoJson = new GeoJson();
        $geoJson->getCoordinates();
    }

    /**
     * @expectedException \RuntimeException
     */
    public function testSetCoordinatesWhenNotInitialized()
    {
        $geoJson = new GeoJson();
        $geoJson->setCoordinates([]);
    }

    public function testFromGeoObject()
    {
        $geoObject = new GeoJson\Point($this->getPointACoords());

        $geoJson = new GeoJson();
        $geoJson->setData($geoObject);
        $geoJson->initialize();

        $this->assertEquals($geoObject, $geoJson->getGeoObject());
        $this->assertNotSame($geoObject, $geoJson->getGeoObject());
    }

    public function testPolygon()
    {
        $polygonString = "13.36881:52.485571
13.345631:52.586905
13.144992:52.586768
13.162901:52.472778
13.36881:52.485571
";

        $polygon = new GeoJson\Polygon($this->getPolygonCoords());
        $this->assertSame($polygonString, (string)$polygon);
        $this->assertSame($this->getPolygonCoordsLastEqualsFirst(), $polygon->getCoordinates());

        $this->assertSame(5, $polygon->key());
        $polygon->rewind();
        $this->assertSame(0, $polygon->key());

        $this->assertInstanceOf('Mongo\Field\GeoJson\Point', $polygon->current());
        $this->assertSame($this->getPointACoords(), $polygon->current()->getCoordinates());

    }

    public function testPolygonFromRing()
    {
        $polygon = new GeoJson\Polygon();
        $polygon->setCoordinates($this->polygonBerlin);

        $this->assertSame(52.45370801465149, $polygon->getFirst()->getLng());
        $this->assertSame(13.319549560546875, $polygon->getFirst()->getLat());

        $this->assertSame(52.45370801465149, $polygon->getLast()->getLng());
        $this->assertSame(13.319549560546875, $polygon->getLast()->getLat());
    }

    public function testPolygonFromFullPolygonArray()
    {
        $polygon = new GeoJson\Polygon();
        $polygon->setCoordinates([$this->polygonBerlin]);

        $this->assertSame(52.45370801465149, $polygon->getFirst()->getLng());
        $this->assertSame(13.319549560546875, $polygon->getFirst()->getLat());

        $this->assertSame(52.45370801465149, $polygon->getLast()->getLng());
        $this->assertSame(13.319549560546875, $polygon->getLast()->getLat());

        $this->assertSame('Polygon', $polygon->getType());

        $provider = new Provider();
        $provider->getGeometry()->setGeoObject($polygon);
        $this->assertSame($polygon, $provider->getGeometry()->getGeoObject());
    }

    public function testPoint()
    {
        $provider = new Provider();
        $provider->getGeometry()->getPoint()->setCoordinates($this->getPointACoords());

        $expectedCoordinates = [
            13.368810,
            52.485571,
        ];

        $expectedCoordinatesReverse = [
            52.485571,
            13.368810,
        ];

        $expectedGeometry = [
            'geometry' => [
                'type' => 'Point',
                'coordinates' => $expectedCoordinates
            ]
        ];

        $this->assertSame($expectedGeometry, $provider->getModified());
        $this->assertSame($expectedGeometry['geometry'], $provider->getGeometry()->toArray());

        $this->assertSame($expectedCoordinates, $provider->getGeometry()->getCoordinates(true));
        $this->assertSame($expectedCoordinatesReverse, $provider->getGeometry()->getCoordinates(false));

        $provider->getGeometry()->setCoordinates($expectedCoordinatesReverse, false);
        $this->assertSame($expectedCoordinates, $provider->getGeometry()->getCoordinates());

        $this->assertSame('Point', $provider->getGeometry()->getType());
        $this->assertSame('13.36881:52.485571', (string)$provider->getGeometry());

        $provider->getGeometry()->setLongitudeFirstValue(true);
        $this->assertSame($expectedCoordinates, $provider->getGeometry()->getCoordinates());

        $provider->getGeometry()->setLongitudeFirstValue(false);
        $this->assertSame($expectedCoordinates, $provider->getGeometry()->getCoordinates());
    }

    /**
     * @expectedException \RuntimeException
     */
    public function testChangeTypeWithoutAloowence()
    {
        $provider = new Provider();
        $provider->getGeometry()->getPoint();
        $provider->getGeometry()->getPolygon();
    }

    public function testChangeTypeWithAloowence()
    {
        $provider = new Provider();
        $provider->getGeometry()->getPoint();

        $provider->getGeometry()->getPolygon(true);
        $this->assertInstanceOf('Mongo\Field\GeoJson\Polygon', $provider->getGeometry()->getGeoObject());

        $provider->getGeometry()->getLineString(true);
        $this->assertInstanceOf('Mongo\Field\GeoJson\LineString', $provider->getGeometry()->getGeoObject());

        $provider->getGeometry()->getMultiPoint(true);
        $this->assertInstanceOf('Mongo\Field\GeoJson\MultiPoint', $provider->getGeometry()->getGeoObject());

        $provider->getGeometry()->getMultiPolygon(true);
        $this->assertInstanceOf('Mongo\Field\GeoJson\MultiPolygon', $provider->getGeometry()->getGeoObject());

        $provider->getGeometry()->getMultiLineString(true);
        $this->assertInstanceOf('Mongo\Field\GeoJson\MultiLineString', $provider->getGeometry()->getGeoObject());
    }

    public function testLineString()
    {
        $lineString = new GeoJson\LineString([$this->getPointACoords(), $this->getPointBCoords()]);

        $this->assertSame('LineString', $lineString->getType());

        $this->assertInstanceOf('Mongo\Field\GeoJson\Point', $lineString->getStart());
        $this->assertInstanceOf('Mongo\Field\GeoJson\Point', $lineString->getEnd());

        $this->assertSame($this->getPointACoords(), $lineString->getStart()->getCoordinates());
        $this->assertSame($this->getPointBCoords(), $lineString->getEnd()->getCoordinates());
        $this->assertSame([$this->getPointACoords(), $this->getPointBCoords()], $lineString->getCoordinates());

        $this->assertSame('13.36881:52.485571 - 13.345631:52.586905', (string)$lineString);

        $this->setExpectedException('\InvalidArgumentException');
        new GeoJson\LineString(['foobar']);
    }

    public function testMultiPoint()
    {
        $coordinates = [
            $this->getPointACoords(),
            $this->getPointBCoords()
        ];
        $multiPoint = new GeoJson\MultiPoint($coordinates);

        $this->assertSame('MultiPoint', $multiPoint->getType());
        $this->assertSame($coordinates, $multiPoint->getCoordinates());
    }

    public function testMultiLineString()
    {
        $coordinates = [
            [$this->getPointACoords(), $this->getPointBCoords()],
            [$this->getPointCCoords(), $this->getPointDCoords()]
        ];
        $multiLineString = new GeoJson\MultiLineString($coordinates);

        $this->assertSame('MultiLineString', $multiLineString->getType());
        $this->assertSame($coordinates, $multiLineString->getCoordinates());
    }

    public function testMultiPolygon()
    {
        $coordinates = [
            [[$this->getPointACoords(), $this->getPointBCoords(), $this->getPointACoords()]],
            [[$this->getPointCCoords(), $this->getPointDCoords(), $this->getPointCCoords()]]
        ];
        $multiPolygon = new GeoJson\MultiPolygon($coordinates);

        $this->assertSame('MultiPolygon', $multiPolygon->getType());
        $this->assertSame($coordinates, $multiPolygon->getCoordinates());
    }

    /**
     * @return array
     */
    public function getPointAGeometry()
    {
        return [
            'geometry' => [
                'type' => 'Point',
                'coordinates' => [
                    $this->getPointACoords()
                ]
            ]
        ];
    }

    /**
     * @return array
     */
    public function getPointACoords()
    {
        return $this->pointACoords;
    }

    /**
     * @return array
     */
    public function getPointBCoords()
    {
        return $this->pointBCoords;
    }

    /**
     * @return array
     */
    public function getPointCCoords()
    {
        return $this->pointCCoords;
    }

    /**
     * @return array
     */
    public function getPointDCoords()
    {
        return $this->pointDCoords;
    }

    /**
     * @return array
     */
    protected function getLineStringCoords()
    {
        return [$this->getPointACoords(), $this->getPointBCoords()];
    }

    /**
     * @return array
     */
    protected function getPolygonCoords()
    {
        return [
            [
                $this->getPointACoords(),
                $this->getPointBCoords(),
                $this->getPointCCoords(),
                $this->getPointDCoords()
            ]
        ];
    }

    /**
     * @return array
     */
    protected function getPolygonCoordsLastEqualsFirst()
    {
        return [
            [
                $this->getPointACoords(),
                $this->getPointBCoords(),
                $this->getPointCCoords(),
                $this->getPointDCoords(),
                $this->getPointACoords(),
            ]
        ];
    }

    /**
     * @return array
     */
    protected function getMultiPointCoords()
    {
        return [$this->getPointACoords(), $this->getPointBCoords(), $this->getPointCCoords()];
    }

    /**
     * @return array
     */
    protected function getMultiLineCoords()
    {
        return [
            $this->getLineStringCoords(),
            [$this->getPointCCoords(), $this->getPointDCoords()],
            [$this->getPointBCoords(), $this->getPointDCoords()]
        ];
    }

    /**
     * @return array
     */
    protected function getMultiPolygonCoords()
    {
        return [
            $this->getPolygonCoords(),
            [[$this->getPointACoords(), $this->getPointBCoords(), $this->getPointDCoords()]],
            [[$this->getPointACoords(), $this->getPointCCoords(), $this->getPointDCoords()]],
        ];
    }
}
