<?php
/**
 * @author Fabian Fuelling <fabian@fabfuel.de>
 * @created: 27.08.14 13:55
 */

namespace Mongo\Test\Functional;

use Mongo\Test\Example\Document\Provider;

class DocumentTest extends DbTestAbstract
{
    public function testChangeValueTwice()
    {
        $data = ['lorem' => 'Ipsum'];
        $provider = new Provider();
        $provider->fromPersistance($data);
        $providers = $this->getCollectionMock('\Mongo\Test\Example\Collection\Providers');

        // Value changed -> modified
        $provider->setLorem('Lorem');
        $this->assertTrue($provider->isModified());

        // Value manually reverted -> not modified
        $provider->setLorem('Ipsum');
        $this->assertFalse($provider->isModified());

        // Value changed -> modified
        $provider->setLorem('Lorem');
        $this->assertTrue($provider->isModified());

        // New Value saved
        $providers->save($provider);

        // Value changed -> modified
        $provider->setLorem('Ipsum');
        $this->assertTrue($provider->isModified());
    }
}
