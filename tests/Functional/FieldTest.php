<?php
/**
 * @author @fabfuel <fabian@fabfuel.de>
 * @created 28.02.14, 20:04 
 */
namespace Mongo\Test\Functional;

use Mongo\Test\Example\Document\User;

class FieldTest extends TestAbstract
{
    public function testDate()
    {
        $user = new User();

        $this->assertNull($user->getAlarm()->getDateTime());
        $this->assertInstanceOf('\DateTime', $user->getAlarm()->getDateTime(true));

        $user->setAlarm(new \DateTime('01.03.2014 06:30'));
        $user->getAlarm()->add(new \DateInterval('P1D'));
        $user->getAlarm()->sub(new \DateInterval('PT24H'));

        $user->getAlarm()->add(new \DateInterval('PT1H'));
        $this->assertEquals($user->getAlarm()->getDateTime(), new \DateTime('01.03.2014 07:30'));

        $this->assertSame('Sat, 01 Mar 2014 07:30:00 +0100', (string)$user->getAlarm());

        $this->assertEquals('01.03.14', $user->getAlarm()->format('d.m.y'));

        $this->assertSame('', $user->getBirthday()->format('d.m.y'));
        $this->assertSame('', (string)$user->getBirthday());

        $user = new User();
        $user->fromPersistance(['alarm' => new \MongoDate()]);
        $this->assertInstanceOf('\DateTime', $user->getAlarm()->getDateTime(false));
    }

    public function testCollection()
    {
        $user = new User();
        $user->fromPersistance($this->user);

        foreach ($user->getChild() as $id => $child) {
            $this->assertEquals(0, $id);
            $this->assertEquals('Max', $child->getFirstname());
        }

        $this->assertEquals(2, count($user->getChild()->getFirst()->getGame()));

        $this->assertEquals($this->user['child'], $user->getChild()->toArray());

        $user->getChild()->getAt(1)->setFirstname('New Child');

        $newArray = $user->getChild()->toArray();
        $newChild = $user->getChild()->getAt(1);

        $this->assertNotEquals(1, $newArray[1]['_id']);

        $this->assertInstanceOf('\Mongo\Test\Example\Document\User\Child', $user->getChild()->getById('12345'));
        $this->assertNull($user->getChild()->getById('123456789'));

        $this->assertTrue($user->getChild()->remove($newChild));
        $this->assertFalse($user->getChild()->removeAt(12345));
    }

    public function testArray()
    {
        $user = new User();

        $this->assertEmpty($user->getSports()->toArray());
        $this->assertSame(0, $user->getSports()->count());

        $user->getSports()->addToSet('tennis');
        $user->getSports()->addToSet('badminton');
        $user->getSports()->addToSet('wakeboard');

        $this->assertEquals(['tennis', 'badminton', 'wakeboard'], $user->getSports()->toArray());
        $this->assertEquals('tennis, badminton, wakeboard', (string)$user->getSports());

        $user->getSports()->addToSet('wakeboard');
        $this->assertEquals(['tennis', 'badminton', 'wakeboard'], $user->getSports()->toArray());

        $user->getSports()->add('wakeboard');
        $this->assertEquals(['tennis', 'badminton', 'wakeboard', 'wakeboard'], $user->getSports()->toArray());

        $user->getSports()->add('5');
        $this->assertEquals(['tennis', 'badminton', 'wakeboard', 'wakeboard', '5'], array_values($user->getSports()->toArray()));

        $this->assertFalse($user->getSports()->has(5, true));
        $this->assertTrue($user->getSports()->has(5, false));

        $this->assertFalse($user->getSports()->addToSet(5, false));
        $this->assertTrue($user->getSports()->addToSet(5, true));

        $removeSucceeded = $user->getSports()->remove(5, true);
        $this->assertEquals(['tennis', 'badminton', 'wakeboard', 'wakeboard', '5'], array_values($user->getSports()->toArray()));
        $this->assertTrue($removeSucceeded);

        $removeFailed = $user->getSports()->remove(5, true);
        $this->assertEquals(['tennis', 'badminton', 'wakeboard', 'wakeboard', '5'], array_values($user->getSports()->toArray()));
        $this->assertFalse($removeFailed);

        $removeSucceeded = $user->getSports()->remove(5, false);
        $this->assertEquals(['tennis', 'badminton', 'wakeboard', 'wakeboard'], array_values($user->getSports()->toArray()));
        $this->assertTrue($removeSucceeded);

        $user->getSports()->remove('wakeboard');
        $this->assertEquals(['tennis', 'badminton', 'wakeboard'], array_values($user->getSports()->toArray()));

        $this->assertSame(3, $user->getSports()->count());

        $this->assertSame('tennis', $user->getSports()->current());
        $this->assertSame('tennis', $user->getSports()->current());
        $this->assertSame(0, $user->getSports()->key());
        $this->assertTrue($user->getSports()->valid());

        $user->getSports()->next();
        $this->assertSame('badminton', $user->getSports()->current());
        $this->assertSame(1, $user->getSports()->key());

        $user->getSports()->next();
        $this->assertSame('wakeboard', $user->getSports()->current());
        $this->assertSame(2, $user->getSports()->key());
        $this->assertTrue($user->getSports()->valid());

        $user->getSports()->next();
        $this->assertFalse($user->getSports()->valid());

        $user->getSports()->rewind();
        $this->assertSame('tennis', $user->getSports()->current());
        $this->assertSame(0, $user->getSports()->key());
    }
}
