<?php
/**
 * @author @fabfuel <fabian@fabfuel.de>
 * @created 13.03.14, 06:52 
 */
namespace Mongo\Test\Functional;

use Mongo\Test\Example\Document\User;

class DefaultValuesTest extends TestAbstract
{
    public function testGetDefaultValues()
    {
        $user = new User();

        $this->assertSame('en', $user->getPreferences()->getLanguage());
        $this->assertSame('utc', $user->getPreferences()->getTimezone());

        $this->assertSame(
            [
                'preferences' => [
                    'language' => 'en',
                    'timezone' => 'utc'
                ]
            ],
            $user->getModified()
        );

        $user->getPreferences()->setLanguage('en');
        $user->getPreferences()->setTimezone('utc');

        $this->assertSame(
            [
                'preferences' => [
                    'language' => 'en',
                    'timezone' => 'utc'
                ]
            ],
            $user->getModified()
        );

        $user->getPreferences()->setLanguage('de');
        $user->getPreferences()->setTimezone('cet');

        $this->assertNotEmpty($user->getModified());
        $this->assertSame(['preferences' =>['language' => 'de', 'timezone' => 'cet']], $user->getModified());
    }
}
