<?php
/**
 * @author Fabian Fülling <fabian@fabfuel.de>
 * @created 01.02.14, 21:57
 */
namespace Mongo\Test\Example\Subdocument;

use Mongo\Subdocument;

class Address extends Subdocument
{
    /**
     * @var string
     */
    protected $street;

    /**
     * @var string
     */
    protected $zip;

    /**
     * @var string
     */
    protected $city;

    /**
     * @var string
     */
    protected $state;

    /**
     * @var string
     */
    protected $country;
}
