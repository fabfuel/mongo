<?php
/**
 * @author @fabfuel <fabian@fabfuel.de>
 * @created 04.02.14 17:55
 */
namespace Mongo\Test\Example\Subdocument;

use Mongo\Subdocument;

class Name extends Subdocument
{
    /**
     * @var string
     */
    protected $first;

    /**
     * @var string
     */
    protected $last;

    /**
     * @var string
     */
    protected $screen;

    /**
     * @var string
     */
    protected $title;

    /**
     * @param string $format
     * @return mixed
     */
    public function getFormatted($format = '{first} {last}')
    {
        return preg_replace_callback('/({(\w+)})/', array($this, 'findName'), $format);
    }

    /**
     * @return mixed
     */
    public function __toString() {
        return $this->getFormatted();
    }

    /**
     * @param $preg
     * @return mixed
     */
    protected function findName($preg)
    {
        $method = 'get' . ucfirst($preg[2]);
        return $this->$method();
    }
}
