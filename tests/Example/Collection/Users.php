<?php
/**
 * @author @fabfuel <fabian@fabfuel.de>
 * @created 30.01.14
 */
namespace Mongo\Test\Example\Collection;

use Mongo\Test\Example\Document\User;
use Mongo\Collection;

/**
 * Class Users
 * @package Collection
 #* @method User findOne
 #* @method User findById
 #* @method User[] find
 */
class Users extends Collection
{
    /**
     * @return string
     */
    public function getCollectionName()
    {
        return 'users';
    }

    /**
     * @return mixed
     */
    public function getDocumentClassName()
    {
        return '\Mongo\Test\Example\Document\User';
    }
}
