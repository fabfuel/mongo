<?php
/**
 * @author @fabfuel <fabian@fabfuel.de>
 * @created 18.02.14, 07:22 
 */
namespace Mongo\Test\Example\Collection;

use Mongo\Test\Example\Document\Order;
use Mongo\Collection;

/**
 * Class Orders
 * @package Mongo\Test\Example\Collection
 * @method Order findOne
 * @method Order findById
 * @method Order[] find
 */
class Orders extends Collection
{
    /**
     * @return string
     */
    public function getCollectionName()
    {
        return 'orders';
    }

    /**
     * @return string
     */
    public function getDocumentClassName()
    {
        return '\Mongo\Test\Example\Document\Order';
    }
}
