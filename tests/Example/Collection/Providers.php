<?php
/**
 * @author @fabfuel <fabian@fabfuel.de>
 * @created 08.02.14, 12:35 
 */
namespace Mongo\Test\Example\Collection;

use Mongo\Test\Example\Document\Provider;
use Mongo\Collection;

/**
 * Class Providers
 * @package Collection
 * @method Provider findOne
 * @method Provider findById
 * @method Provider[] find
 */
class Providers extends Collection
{
    /**
     * @return string
     */
    public function getCollectionName()
    {
        return 'providers';
    }

    /**
     * @return string
     */
    public function getDocumentClassName()
    {
        return '\Mongo\Test\Example\Document\Provider';
    }
}
