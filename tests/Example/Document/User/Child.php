<?php
/**
 * @author Fabian Fülling <fabian@fabfuel.de>
 * @created 01.02.14, 21:57
 */
namespace Mongo\Test\Example\Document\User;

use Mongo\Subdocument;

class Child extends Subdocument
{
    /**
     * @var string
     */
    protected $firstname;

    /**
     * @var int
     */
    protected $age;

    /**
     * @var \Mongo\Test\Example\Document\User\Child\Game[]
     */
    protected $game;
}
