<?php
/**
 * @author @fabfuel <fabian@fabfuel.de>
 * @created 02.02.14, 21:44 
 */
namespace Mongo\Test\Example\Document\User\Child;

use Mongo\Subdocument;

class Game extends Subdocument
{
    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $type;
}
