<?php
/**
 * @author @fabfuel <fabian@fabfuel.de>
 * @created 12.03.14, 20:23 
 */
namespace Mongo\Test\Example\Document\User;

use Mongo\Subdocument;

class Preferences extends Subdocument
{
    /**
     * @var string
     */
    protected $language = 'en';

    /**
     * @var string
     */
    protected $timezone = 'utc';
}
