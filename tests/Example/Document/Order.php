<?php
/**
 * @author @fabfuel <fabian@fabfuel.de>
 * @created 18.02.14, 07:22 
 */
namespace Mongo\Test\Example\Document;

use Mongo\Document;

class Order extends Document
{
    /**
     * @var \MongoId
     * @reference \Mongo\Test\Example\Collection\Users
     */
    protected $user;

    /**
     * @var \Mongo\Field\Date
     */
    protected $timestamp;

    /**
     * @var float
     */
    protected $amount;
}
