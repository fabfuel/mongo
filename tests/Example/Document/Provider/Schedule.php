<?php
/**
 * @author @fabfuel <fabian@fabfuel.de>
 * @created 08.02.14, 12:52 
 */
namespace Mongo\Test\Example\Document\Provider;

use Mongo\Subdocument;

class Schedule extends Subdocument
{
    /**
     * @var int
     * @alias valid_from
     */
    protected $validFrom;

    /**
     * @var int
     * @alias valid_until
     */
    protected $validUntil;

    /**
     * @var \Document\Provider\Schedule\Availability[]
     */
    protected $availability;
}
