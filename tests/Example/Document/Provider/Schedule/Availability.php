<?php
/**
 * @author @fabfuel <fabian@fabfuel.de>
 * @created 08.02.14, 17:25 
 */
namespace Mongo\Test\Example\Document\Provider\Schedule;

use Mongo\Subdocument;

class Availability extends Subdocument
{
    /**
     * @var string
     */
    protected $weekday;

    /**
     * @var int
     */
    protected $from;

    /**
     * @var int
     */
    protected $until;
}
