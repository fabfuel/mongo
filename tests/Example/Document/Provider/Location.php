<?php
/**
 * @author @fabfuel <fabian@fabfuel.de>
 * @created 08.02.14, 12:52 
 */
namespace Mongo\Test\Example\Document\Provider;

use Mongo\Field\GeoJson;
use Mongo\Subdocument;

/**
 * Class Location
 * @package Document\Provider
 * @method GeoJson getPoint
 * @method Location setPoint(GeoJson $geoJson)
 * @method GeoJson getLine
 * @method Location setLine(GeoJson $geoJson)
 * @method GeoJson getPolygon
 * @method Location setPolygon(GeoJson $geoJson)
 */
class Location extends Subdocument
{
    /**
     * @var string
     */
    protected $title;

    /**
     * @var \Mongo\Field\GeoJson
     */
    protected $point;

    /**
     * @var \Mongo\Field\GeoJson
     */
    protected $line;

    /**
     * @var \Mongo\Field\GeoJson
     */
    protected $polygon;
}
