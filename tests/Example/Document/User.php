<?php
/**
 * @author Fabian Fülling <fabian@fabfuel.de>
 * @created 31.01.14 14:45
 */
namespace Mongo\Test\Example\Document;

use Mongo\Document;
use Mongo\Field\Date;
use Mongo\Field\ArrayField;
use Document\User\ChildCollection;
use Subdocument\Address;
use Subdocument\Name;

/**
 * Class User
 * @package Document
 * @method ChildCollection getChild
 * @method Name getName
 * @method Address getAddress
 * @method Date getAlarm
 * @method Date getBirthday
 * @method float getFloat
 * @method ArrayField getSports
 * @method ArrayField getHobby
 */
class User extends Document
{
    /**
     * @var \Mongo\Test\Example\Subdocument\Name
     */
    protected $name;

    /**
     * @var \Mongo\Test\Example\Subdocument\Address
     * @alias location
     */
    protected $address;

    /**
     * @var \Mongo\Test\Example\Document\User\Child[]
     */
    protected $child;

    /**
     * @var \Mongo\Test\Example\Document\User\Alarm
     */
    protected $alarm;

    /**
     * @var \Mongo\Field\Date
     */
    protected $birthday;

    /**
     * @var float
     */
    protected $float;

    /**
     * @var array
     */
    protected $sports;

    /**
     * @var array
     */
    protected $hobby;

    /**
     * @var int
     */
    protected $changes;

    /**
     * @var \Mongo\Test\Example\Document\User\Preferences
     */
    protected $preferences;
}
