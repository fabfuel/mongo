<?php
/**
 * @author @fabfuel <fabian@fabfuel.de>
 * @created 29.10.14, 07:22
 */
namespace Mongo\Test\Example\Document;

use Mongo\Document;

/**
 * Class Reference
 * @package Document
 * @method \MongoId getReferenceId
 * @method void setReferenceId(\MongoId $mongoId)
 * @method string getReferenceCollection(\MongoId $mongoId)
 * @method void setReferenceCollection(string $referenceCollection)
 */
class Reference extends Document
{
    /**
     * @var \MongoId
     */
    protected $referenceId;

    /**
     * @var string
     */
    protected $referenceCollection;
}
