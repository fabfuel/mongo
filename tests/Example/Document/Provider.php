<?php
/**
 * @author @fabfuel <fabian@fabfuel.de>
 * @created 08.02.14, 12:38 
 */
namespace Mongo\Test\Example\Document;

use Document\Provider\Location;
use Document\Provider\Schedule;
use Mongo\Document;
use Mongo\Field\GeoJson\GeoObject;
use Mongo\Field\GeoJson;
use Subdocument\Address;
use Subdocument\Name;

/**
 * Class Provider
 * @package Document
 * @method Name getName
 * @method Address getAddress
 * @method Location getLocation
 * @method Schedule[] getSchedule
 * @method GeoJson getGeometry()
 * @method Provider setGeometry(GeoJson $geoJson)
 */
class Provider extends Document
{
    /**
     * @var \Mongo\Test\Example\Subdocument\Name
     */
    protected $name;

    /**
     * @var \Mongo\Test\Example\Subdocument\Address
     */
    protected $address;

    /**
     * @var \Mongo\Test\Example\Document\Provider\Location
     */
    protected $location;

    /**
     * @var \Mongo\Test\Example\Document\Provider\Schedule[]
     */
    protected $schedule;

    /**
     * @var string
     */
    protected $lorem;

    /**
     * @var string
     */
    protected $foo;

    /**
     * @var array
     */
    protected $tags;

    /**
     * @var \Mongo\Field\GeoJson
     */
    protected $geometry;

    /**
     * @return mixed
     */
    public function __toString() {
        return $this->getName()->getFormatted();
    }
}
