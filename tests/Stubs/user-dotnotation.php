<?php

return [
    'name.first' => 'John',
    'name.last' => 'Doe',
    'name.screen' => 'Johnny',
    'name.title' => 'Mr.',
    'address.street' => '5th Avenue',
    'address.zip' => '4465',
    'address.city' => 'New York',
    'address.state' => 'New Jersey',
    'address.country' => 'US',
    'child.0._id'  => '12345',
    'child.0.firstname' => 'Max',
    'child.0.age' => 7,
    'child.0.game.0._id'  => '12345',
    'child.0.game.0.name' => 'GameBoy',
    'child.0.game.0.type' => 'video game',
    'child.0.game.1._id'  => '12345',
    'child.0.game.1.name' => 'Monopoly',
    'child.0.game.1.type' => 'parlor game',
    'alarm' => new \MongoDate(1393651800),
    'birthday' => new \MongoDate(954590400),
];
