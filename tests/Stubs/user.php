<?php

return [
    'name' => [
        'first' => 'John',
        'last' => 'Doe',
        'screen' => 'Johnny',
        'title' => 'Mr.',
    ],
    'address' => [
        'street' => '5th Avenue',
        'zip' => '4465',
        'city' => 'New York',
        'state' => 'New Jersey',
        'country' => 'US',
    ],
    'child' => [
        0 => [
            '_id'  => '12345',
            'firstname' => 'Max',
            'age' => 7,
            'game' => [
                0 => [
                    '_id'  => '12345',
                    'name' => 'GameBoy',
                    'type' => 'video game',
                ],
                1 => [
                    '_id'  => '12345',
                    'name' => 'Monopoly',
                    'type' => 'parlor game',
                ]
            ]
        ]
    ],
    'alarm' => new \MongoDate(1393651800),
    'birthday' => new \MongoDate(954590400),
];
